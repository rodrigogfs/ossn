function [F, Phi, Z, dist2] = rbfforward(net, X)
% Calculate squared norm matrix, of dimension (ndata, ncentres)
dist2 = squaredDistTwoSets(X, net.c);
% Calculate width factors: net.wi contains squared widths
% Now compute the activations
Phi = exp(- (dist2 ./ (2*net.width) ));
Z = Phi*net.w + net.bias;
F = sigmoid(Z);
end