function params = runWithBestModels(paramfile,outfile,type,label,data,seq,method,semisup)
pp=load(paramfile); % best models have already been collected
params = pp.params;
dart = '/rds/homes/m/minkull/spdisc-data/rodrigo/data/nonuni/%s_matlab/stream%s.csv';
lart = '/rds/homes/m/minkull/spdisc-data/rodrigo/data/nonuni/%s_matlab/lab_%s_stream%s_nonuni_l%s.csv';
% dart = '/rds/homes/m/minkull/spdisc-data/rodrigo/data/%s_matlab/stream%s.csv';
% lart = '/rds/homes/m/minkull/spdisc-data/rodrigo/data/%s_matlab/lab_%s_stream%s_uni_l%s.csv';
% dart = 'C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\%s\\stream%s.csv';
% lart = 'C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\%s\\lab_%s_stream%s_uni_l%s.csv';
dreal = '/rds/homes/m/minkull/spdisc-data/rodrigo/data/nonuni/real_matlab/train_%s.csv';
lreal = '/rds/homes/m/minkull/spdisc-data/rodrigo/data/nonuni/real_matlab/lab_train_%s_nonuni_l%s.csv';
% dreal = '/rds/homes/m/minkull/spdisc-data/rodrigo/data/realstreams/train_%s.csv';
% lreal = '/rds/homes/m/minkull/spdisc-data/rodrigo/data/realstreams/lab_train_%s_uni_l%s.csv';
% dextra = '../data/extra/extra_matlab/%s.csv';
% lextra = '../data/extra/extra_matlab/lab_train_%s_l%s.csv';
% dvisual = '/rds/homes/m/minkull/spdisc-data/rodrigo/data/visual/train_%s.csv';
% lvisual = '/rds/homes/m/minkull/spdisc-data/rodrigo/data/visual/lab_train_%s_uni_l%s.csv';
dvisual = '/rds/homes/m/minkull/spdisc-data/rodrigo/data/visual/train_%s.csv';
lvisual = '/rds/homes/m/minkull/spdisc-data/rodrigo/data/visual/lab_train_%s_nonuni_l%s.csv';
% executing final run
if strcmpi(type,'real')
    searchdata = sprintf(dreal,data);
    searchlab = sprintf(lreal,data,label);
elseif strcmpi(type,'visual')
    searchdata = sprintf(dvisual,data);
    searchlab = sprintf(lvisual,data,label);
elseif strcmpi(type,'gradual') || strcmpi(type,'abrupt') 
    searchdata = sprintf(dart,type,seq);
    searchlab = sprintf(lart,type,type,seq,label);
elseif strcmpi(type,'extra') 
    searchdata = sprintf(dextra,data);
    searchlab = sprintf(lextra,data,label);
else
    error('Invalid stream type');
end
% all sequences of files with dataset
%params.(t{1}).(d{1}).(m{1}).nhidden = params.(t{1}).(d{1}).(m{1}).nhidden + 200;
if strcmpi(type,'real')
    pt='real';
elseif strcmpi(type,'gradual') || strcmpi(type,'abrupt') 
    pt='art';
elseif strcmpi(type,'extra')
    pt='extra';
elseif strcmpi(type,'visual')
    pt='visual';
else
    error('Invalid stream type');
end
trim_data_name = replace(data,'-','');
[preqvector, ~, ~, eta_log, centres_log, pred_log, truelabels_log, time_log]=runOnlineSSL(searchdata,searchlab,semisup,params.(pt).(trim_data_name).(method));
dlmwrite(outfile,preqvector*100);
save([outfile '_sensitivity_logs.mat'], "eta_log", "centres_log");
save([outfile '_pred_time_logs.mat'], "pred_log", "truelabels_log", "time_log");
end