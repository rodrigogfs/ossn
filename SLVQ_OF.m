function net = SLVQ_OF(net, fp, lBatch, lY, uBatch)
% OF means Opposing Forces to avoid centering of centres in a single point
% This is competitive hebbian learning. It considers that centres have classes 
% How to assign classes to centres?
% A mix of LVQ3 and Neural Gas in order to deal with semisupervised
% learning
% This "FLEX" version accepts both labelled and unlabelled data or just one of these types
% of data. The other version only adapts when both are present
c = size(net.c,1);
[n, d] = size(uBatch);
if ~isempty(uBatch)
    %% updating widths with the new batch
    dist2 = squaredDistTwoSets(uBatch,net.c);
    widths = (fp.sigmarate*sqrt(min(dist2))).^2;
    updaterates = exp( - dist2 ./ (2*widths+1e-5) );
    %% updating centres according to unlabelled data
    idxCentres = kron((1:c)',ones(n,1));
    idxTrain = kron(ones(1,c),(1:n) );
    Uupdates = uBatch(idxTrain,:) - net.c(idxCentres,:);
    Uupdates = updaterates(:).*Uupdates;
    idxAcc = idxCentres(:,ones(1,d));
    idxAcc = [idxAcc(:) kron((1:d)',ones(c*n,1))];
    Uupdates = accumarray(idxAcc, Uupdates(:)) ./ (sum(updaterates)+1e-5)';
    % Uupdates = Uupdates ./ (sqrt(sum(Uupdates.^2,2))+1e-5); %normalizing
else
    Uupdates = 0;
end
if ~isempty(lBatch)
    %% updating centres according to labelled data
    % assigning labels to prototypes using the network output
    centreClasses = (rbfforward(net,net.c)>0.5);
    n = size(lBatch,1);
    dist2 = squaredDistTwoSets(lBatch,net.c);
    widths = (fp.sigmarate*sqrt(min(dist2))).^2;
    updaterates = exp( - dist2 ./ (2*widths+1e-5) );
    idxCentres = kron((1:c)',ones(n,1));
    idxTrain = kron(ones(1,c),(1:n) )';
    Lupdates = lBatch(idxTrain,:) - net.c(idxCentres,:);
    % LVQ depends on the correct class assignments
    updatesign = (lY(idxTrain) == centreClasses(idxCentres))*2 - 1;
    Lupdates = (updatesign.*updaterates(:)).*Lupdates;
    idxAcc = idxCentres(:,ones(1,d));
    idxAcc = [idxAcc(:) kron((1:d)',ones(c*n,1))];
    Lupdates = accumarray(idxAcc, Lupdates(:)) ./ (sum(updaterates)+1e-5)';
    % Lupdates = Lupdates ./ (sqrt(sum(Lupdates.^2,2))+1e-5); % normalizing
else
    Lupdates=0;
end
%% Using the neighbours to spread the centres
dist2 = squaredDistTwoSets(net.c,net.c);
dist2(1:c+1:end) = Inf;
[~,idx] = min(dist2);
Cupdates = net.c - net.c(idx,:); % this is equal to -1 * (NN - Centre)
% Cupdates =  Cupdates ./ (sqrt(sum(Cupdates.^2,2))+1e-5);
Cupdates = rand(size(Cupdates)).*Cupdates;
%% Updating centres
updates = Lupdates + Uupdates + Cupdates;
% updates = updates ./ (sqrt(sum(updates.^2,2))+1e-5);
net.c = net.c + (fp.eta*updates);
end