function net = ManifoldBased_SLVQ(net, lBatch, yBatch, uBatch)
dist2L = squaredDistTwoSets(lBatch,net.c);
dist2U = squaredDistTwoSets(uBatch,net.c);
[~,nnL] = min(dist2L,[],2);
[~,nnU] = min(dist2U,[],2);
l = size(lBatch,1);
u = size(uBatch,1);
for i=1:net.nhidden
    regionUidx = (nnU == i);
    regionLidx0 = ((nnL == i) & (yBatch == 0));
    regionLidx1 = ((nnL == i) & (yBatch == 1));
    if sum(regionLidx0) > sum(regionLidx1)
        regionMaj = lBatch(regionLidx0,:);
        regionMin = lBatch(regionLidx1,:);
    else
        regionMaj = lBatch(regionLidx1,:);
        regionMin = lBatch(regionLidx0,:);
    end  
    regionU = uBatch(regionUidx,:);    
    sumU = sum(regionU,1) / u;
    sumMaj = sum(regionMaj,1) / l;
    sumMin = sum(regionMin,1) / l;
    scaling = ((size(regionMaj,1) - size(regionMin,1)) / l) + (size(regionU,1) / u);
    if scaling>0
        net.c(i,:) = (sumMaj - sumMin + sumU) / scaling;
    end
end