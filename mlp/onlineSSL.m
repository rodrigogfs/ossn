function [preqacc, net, losses] = onlineSSL(XTrain,YTrain,labelmask,fp,drawplot)
%ONLINESSL This is the training for online SSL for a single batch
%   This includes unsupervised and semissupervised training
%   XTtrain is the complete data stream
%   YTrain is the true labels
%   labels are binary masks

%% creating targets from class labels
disp(datetime); % keeping track of time
n = size(XTrain,1);
d = size(XTrain,2);
classes = sort(unique(YTrain)); % this should be 0 and 1
numclasses = length(classes); % this should be 2
t = fp.batchsize;
preqacc = zeros(1,n);
preqS = 0;
preqN = 0;
fadingfactor = 0.999;
%% generate MLP
net = generateMLP(fp,d);
%% stream loop
while t<n
    window = max([1 t+1-fp.batchsize]):t;
    XBatch = XTrain(window,:);
    YBatch = YTrain(window);
    labelsBatch = labelmask(window);
    lBatch = XBatch(labelsBatch,:);
    uBatch = XBatch(~labelsBatch,:);
    yBatch = YBatch(labelsBatch);
    
    %% SemiSupervised training
    if strcmpi(fp.methodw,'NR')
        net = trainOSSL_NR(net, fp, lBatch, yBatch, uBatch);
    elseif strcmpi(fp.methodw,'NAG')
        net = trainOSSL_NAG(net, fp, lBatch, yBatch, uBatch);
    else
        [net, loss] = trainOSSL_SGD(net, fp, lBatch, yBatch, uBatch);
    end
    losses(t)=loss;
    %% Computing prequential errors
    XTestBatch = XTrain(t+1,:);
    YTestBatch = YTrain(t+1);
    testf = mlpforward(net,XTestBatch);
    Idx = testf>0.5;
    testacc = (Idx==YTestBatch);
    preqS = testacc + fadingfactor*preqS;
    preqN = 1 + fadingfactor*preqN;
    preqacc(t+1)=preqS/preqN;
    %% incremeting time steps
    t = t + 1;
    if mod(t,100)==0
        fprintf('Time step %d - Accuracy %.2f \n',t, preqacc(t));
        disp(datetime); % keeping track of time
        if drawplot==1
            plot(preqacc(2:t)); drawnow;
        end
    end
    %% Plotting the output
    if drawplot>1 && mod(t,100)==0
        marker = {'+' 'x' 'o' '*' '.' 'p' 's' 'd'};
        color = {'r' 'b' 'm' 'c' 'k' 'g'};
        % plotting the NN test output
        figure(1); hold on;
        % plotting test decision boundary
        for pl=0:1
            plot(XTestBatch((Idx==pl),1),XTestBatch((Idx==pl),2),[marker{mod(pl,8)+1} color{mod(pl,6)+1}],'LineWidth',2.5);
        end
        plot(XTrain(:,1),XTrain(:,2),'+k');
        title(['Predictions It:' num2str(t)]);
        testmin = min(XTrain);
        testmax = max(XTrain);
        axis([testmin(1) testmax(1) testmin(2) testmax(2)]);
%         axis tight
        if d<=2
            h = gca;
            a = get(h,'Xlim');
            b = get(h,'Ylim');
            box = [a b];
            % Visualise the results
            gsteps        = 50;
            range1        = box(1):(box(2)-box(1))/(gsteps-1):box(2);
            range2        = box(3):(box(4)-box(3))/(gsteps-1):box(4);
            [grid1, grid2]    = meshgrid(range1,range2);
            Xgrid        = [grid1(:) grid2(:)];
            output = mlpforward(net,Xgrid);
            contour(range1,range2,reshape(output,size(grid1)),[0.5 0.5],'-','Color','r','LineWidth',2.5);
        end
        for pl=1:numclasses
            plot(XBatch(labelsBatch&(Idx==classes(pl)),1),XBatch(labelsBatch&(Idx==classes(pl)),2),'d','LineWidth',2,'MarkerEdgeColor','k','MarkerFaceColor',color{mod(pl,6)+3},'MarkerSize',7);
        end
        hold off;
        drawnow; if t<n, clf; end
    end
end
end