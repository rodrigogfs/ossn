net = generateMLP(fp,2);
clear losses
labels = logical(randi([0,1],460,1));
for i=1:1000
[net, loss] = trainOSSL_SGD(net, fp, X(labels,1:2), X(labels,3), X(~labels,1:2));

losses(i)=loss;
end
f = mlpforward(net,X(:,1:2));
scatter(X(:,1),X(:,2),20,f); drawnow;
figure;
plot(losses);

% clear net losses