function [F, Z, Phi] = mlpforward(net,X)
%MLPFORWARD forward phase of training
Phi = cell(1,net.nlayers); % transformed inputs
Z = cell(1,net.nlayers);
Phi{1} = X; % adding ones for biases
% input layer
for i=1:net.nlayers-1 % only hidden layers
    Z{i} = Phi{i}*net.layers{i}.w + net.layers{i}.bias; % bias is NOT included as last weight
    Phi{i+1} = tanh(Z{i}); % activation with biases
end
% output layer (this is to avoid 'if' inside the loop above)
Z{end} = Phi{end}*net.layers{end}.w + net.layers{end}.bias; % bias is NOT included as last weight
F = sigmoid(Z{end}); % activation output layer
end