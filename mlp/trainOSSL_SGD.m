function [net, loss] = trainOSSL_SGD(net, fp, Xlab, lY, Xunl)
% This is the supervised part of the training algorithm (standard SGD)
% and l2 regularization
%% Calculating similarities for pseudolabels
% The labelled instances are considered for the neighborhood
% This only works with at leats minibatches
dist2 = squaredDistTwoSets(Xunl,[Xlab; Xunl]);
dist2(dist2==0)=Inf;
sigma = fp.sigmamanif*sqrt(min(dist2)) + 1e-8;
S = exp(- dist2 ./ (2*sigma.^2) );
%% forward phase
[lF, lZ, lPhi] = mlpforward(net,Xlab);
[uF, uZ, uPhi] = mlpforward(net,Xunl);
% each layer delta error is a row vector n x 1
lDelta = cell(1,net.nlayers); % layerwise estimated errors
uDelta = cell(1,net.nlayers); % layerwise estimated errors
%% pseudo-labels
u = ( (S*[lY; uF]) ./ (sum(S,2)+1e-8) ); % with safeguard
numL = size(lF,1); numU=size(uF,1);

%% backpropagation for each layer
%% output layer first
lDelta{end} = (lF-lY) / numL; %(lF-lY); % only one output neuron so delta is nx1
uDelta{end} = (fp.lambda/numU) * (uF-u); %(uF-u);
lG = lPhi{end}' * lDelta{end}; % this is where the sum of instances occur
uG = uPhi{end}' * uDelta{end}; % h1 x n  n x h2 = h1 x h2
if isempty(lG); lG=0; end % if there is no labelled instance in this batch
% adding all terms of the gradient including L2 regularization
G = lG + uG;
G_b = sum(lDelta{end},1) + sum(uDelta{end},1); % bias learning
% updating weights
net.layers{end}.w = (1-fp.etaw*fp.alpha)*net.layers{end}.w - fp.etaw*G; % update with L2
net.layers{end}.bias = net.layers{end}.bias - fp.etaw*G_b; % update WITHOUT L2 regularization
%% all hidden layers backwards
for i=net.nlayers-1:-1:1
    %% gradient for each layer
    lDelta{i} = (1 - tanh(lZ{i}).^2) .* (lDelta{i+1} * net.layers{i+1}.w'); % no bias no last weight
    uDelta{i} = (1 - tanh(uZ{i}).^2) .* (uDelta{i+1} * net.layers{i+1}.w');
    lG = lPhi{i}' * lDelta{i}; % this is where the sum of instances occur
    uG = uPhi{i}' * uDelta{i}; % h1 x n  n x h2 = h1 x h2
    G_b = sum(lDelta{i},1) + sum(uDelta{i},1); % bias learning
    if isempty(lG); lG=0; end % if there is no labelled instance in this batch
    % adding all terms of the gradient
    G = lG + uG;
    %% updating weights
    net.layers{i}.w = (1-fp.etaw*fp.alpha)*net.layers{i}.w - fp.etaw*G;
    net.layers{i}.bias = net.layers{i}.bias - fp.etaw*G_b; % update WITHOUT L2 regularization
end
% current loss
lF = mlpforward(net,Xlab);
uF = mlpforward(net,Xunl);
lossU = - fp.lambda * sum( u.*log(uF) + (1-u).*log(1-uF) ) / numU;
lossL = - sum( lY.*log(lF) + (1-lY).*log(1-lF) ) / numL;
lossW = fp.alpha*L2Reg(net)/2;
if ~isfinite(lossL); lossL=0; end
loss = lossL + lossU + lossW; % + (fp.alpha*L2Reg(net)/2);
end

function [L2, sumW] = L2Reg(net)
sumW=0;
L2=0;
for l=net.layers
    sumW = sumW + sum(sum(l{1}.w));
    L2 = L2 + sum(sum(l{1}.w.^2));
end
end
