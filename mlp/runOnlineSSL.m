function [preqacc, preqstd] = runOnlineSSL(Xfile,labelsFile,resultFile,method,methodw,...
    sigmarate,sigmamanif,eta,etaw,beta,alpha,epochs,lambda,width,nlayers,nhidden,batchsize,seed,fp)
%RUNONLINESSL Runs onlineSSL once without using a struct as parameter container.
%Ideal for paralelizing randomized search.
if isempty(fp)
    % Setting the random seed
    rng(seed);
    % building parameters struct
    fp=struct;
    fp.sigmarate=sigmarate;
    fp.sigmamanif=sigmamanif;
    fp.eta=eta;
    fp.alpha=alpha;
    fp.epochs=epochs;
    fp.lambda=lambda;
    fp.width=width;
    fp.nlayers=nlayers;
    fp.nhidden=nhidden;
    fp.batchsize=batchsize;
    fp.method=method;
    fp.methodw=methodw;
    fp.etaw=etaw;
    fp.beta=beta;
    fp.seed=seed;
else
    rng(fp.seed);
end
%% reading files and preparing data
X = dlmread(Xfile,',',1,0);
labelmask = logical(load(labelsFile));
Y = X(:,end);
X = X(:,1:end-1);
%% running onlinessl
preqvector = onlineSSL(X,Y,labelmask,fp,1);
preqacc = mean(preqvector(2:end)); % sampling at every 500 steps
preqstd = std(preqvector(2:end));
if ~isempty(resultFile)
    save(resultFile,'fp','preqacc','preqstd');
end
end
