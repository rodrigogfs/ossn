function net = generateMLP(fp,d)
% generates a MLP for binary classification with A SINGLE output. 
net = struct();
net.nin = d;
net.nout = 1; % Binary classification with one output neuron
net.nhidden = fp.nhidden;
net.nlayers = fp.nlayers;
net.layers = cell(1,net.nlayers);
for i=1:net.nlayers
    newlayer = struct();
    if i==1 % first hidden layer
        newlayer.w = 0.05*randn(net.nin,net.nhidden);
        newlayer.bias = 0.05*randn(1,net.nhidden);
    elseif i==net.nlayers % output layer
        newlayer.w = 0.05*randn(net.nhidden,net.nout);
        newlayer.bias = 0.05*randn(1,net.nout);
    else % other layers
        newlayer.w = 0.05*randn(net.nhidden,net.nhidden); 
        newlayer.bias = 0.05*randn(1,net.nhidden);
    end
    net.layers{i} = newlayer;
end
end