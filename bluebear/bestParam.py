# Author: Chun Wai Chiu (michaelchiucw@gmail.com)
# Script for parameter tuning (Getting the best performing one)

import pandas as pd
import matplotlib.pyplot as plt
from os import listdir

def find_csv_filenames(path_to_dir, suffix=".csv" ):
    filenames = listdir(path_to_dir)
    return [ filename for filename in filenames if filename.endswith( suffix ) ]

def getBestParam():
	fileList = find_csv_filenames("./")
	bestScore = 0
	bestFile = ""
	for file in fileList:
		try:
			data = pd.read_csv(file)
		except:
			print(file)
		y = data['classifications correct (percent)']
		accuracyTotal = 0
		for accuracy in y:
			accuracyTotal += accuracy
		accuracy = accuracyTotal/len(y)
		if accuracy > bestScore:
			bestFile = file
			bestScore = accuracy
	
	for file in fileList:
		try:
			data = pd.read_csv(file)
		except:
			print(file)
		y = data['classifications correct (percent)']
		accuracyTotal = 0
		for accuracy in y:
			accuracyTotal += accuracy
		accuracy = accuracyTotal/len(y)
		if accuracy == bestScore:
			print file
			print accuracy
	
getBestParam()
