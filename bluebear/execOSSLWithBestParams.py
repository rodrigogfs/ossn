from subprocess import call
from os import remove
from os.path import join
from sys import argv

boilerplate = ("#!/bin/bash\n"
"#SBATCH --ntasks 1\n"
"#SBATCH --time 240:0:0\n"
"#SBATCH --mem 16G\n"
"#SBATCH --qos bbdefault\n"
"#SBATCH --mail-type NONE\n"
"set -e\n"
"cd /rds/homes/m/minkull/spdisc-data/rodrigo/onlinessl\n"
"module purge; module load bluebear\n"
"module load MATLAB/2019b\n"
"\n")

prefix = "matlab -nodisplay -nojvm -r "
paramfile = argv[1] # path is relative to the MATLAB directory
tempdir = './tesnonuni' # Temp Result directory
'''
streams = {'abrupt':{'Sine':[1,2],'Agrawal':[3,4,5,6],'SEA':[7,8],'STAGGER':[9,10]},
           'gradual':{'Sine':[1,2],'Agrawal':[3,4,5,6],'SEA':[7,8],'STAGGER':[9,10]},
           'real':{'elec2':[1],'noaa':[1],'powersupplyDayNight':[1],'sensorClasses29and31':[1],'weatherAUSprep':[1]}
           }
'''
streams = {'abrupt':{'Sine':[1,2],'SEA':[7,8],'STAGGER':[9,10],'Agrawal':[3,4,5,6]},
           'gradual':{'Sine':[1,2],'SEA':[7,8],'STAGGER':[9,10],'Agrawal':[3,4,5,6]},
           'real':{'elec2':[1],'noaa':[1],'powersupplyDayNight':[1],'sensorClasses29and31':[1]},
           #'extra':{'g241d':[1],'digit1':[1]}
           'visual':{'outdoor-classes-0-19':[1],'rialto-classes-0-4':[1]}
           }
labelsextra = ['10', '100']
labelsrates = ['5', '10', '20']
#methods = ['manifold', 'purecomp', 'slvq', 'purecomp_OF', 'SLVQ_OF', 'id'];
methods = ['manifold'];
semisup = 1

for t,s in streams.items():
    if t=='extra':
        labels = labelsextra
    else:
        labels = labelsrates
    for label in labels:
        for data,seqs in s.items():
            for seq in seqs:
                for m in methods:
                    out = join(tempdir, "{d}_{t}_{l}_{s}_{m}.csv".format(t=t,l=label,d=data,s=seq,m=m))
                    cmd = "\"runWithBestModels(\'{paramfile}\', \'{outfile}\', \'{typesidx}\', \'{labelsidx}\', \'{dataidx}\', \'{seqsidx}\', \'{methodsidx}\', {semisup});\"\n".format(
                        paramfile=paramfile,outfile=out,typesidx=t,labelsidx=label,dataidx=data,seqsidx=str(seq),methodsidx=m,semisup=semisup)
                    cmd = boilerplate + prefix + cmd
                    job = "{d}_{t}_{l}_{s}_{m}.sh".format(t=t,l=label,d=data,s=seq,m=m)
                    with open(job, 'w') as fid:
                        fid.write(cmd)
                    call(["sbatch", job])

