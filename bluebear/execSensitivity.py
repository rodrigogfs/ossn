from subprocess import call
from os import remove
from os.path import join
from sys import argv

boilerplate = ("#!/bin/bash\n"
"#SBATCH --ntasks 1\n"
"#SBATCH --time 240:0:0\n"
"#SBATCH --mem 8G\n"
"#SBATCH --qos bbdefault\n"
"#SBATCH --mail-type NONE\n"
"set -e\n"
"cd /rds/projects/2018/minkull-spdisc/rodrigo/onlinessl/\n"
"module purge; module load bluebear\n"
"module load MATLAB/2019b\n"
"\n")

prefix = "matlab -nodisplay -nojvm -r "
paramfile = argv[1] # path is relative to the MATLAB directory
tempdir = '../jobs/temp/' # Temp Result directory
'''
streams = {'abrupt':{'Sine':[1,2],'Agrawal':[3,4,5,6],'SEA':[7,8],'STAGGER':[9,10]},
           'gradual':{'Sine':[1,2],'Agrawal':[3,4,5,6],'SEA':[7,8],'STAGGER':[9,10]},
           'real':{'elec2':[1],'noaa':[1],'powersupplyDayNight':[1],'sensorClasses29and31':[1],'weatherAUSprep':[1]}
           }
'''
'''
streams = {'abrupt':{'Sine':[1,2],'SEA':[7,8],'STAGGER':[9,10],'Agrawal':[3,4,5,6]},
           'gradual':{'Sine':[1,2],'SEA':[7,8],'STAGGER':[9,10],'Agrawal':[3,4,5,6]},
           'real':{'elec2':[1],'noaa':[1],'powersupplyDayNight':[1],'sensorClasses29and31':[1]},
           'extra':{'g241d':[1],'digit1':[1]}
           }
'''

streams = {'abrupt':{'Sine':[2],'SEA':[8]},
           'real':{'elec2':[1]}
           }

labelsextra = ['10', '100']
labelsrates = ['20']
hyperp = 'nhidden'
#hypervalues = [1]+list(range(500,10001,500))
hypervalues = [1]+list(range(100,2001,100))+list(range(2500,10001,500))
#methods = ['manifold', 'purecomp', 'slvq', 'purecomp_OF', 'SLVQ_OF', 'id'];
methods = ['manifold'];
semisup = 1

for t,s in streams.items():
    if t=='extra':
        labels = labelsextra
    else:
        labels = labelsrates
    for label in labels:
        for data,seqs in s.items():
            for seq in seqs:
                for m in methods:
                    for hyperv in hypervalues:
                        out = join(tempdir, "nonuni_{hp}_{d}_{t}_{l}_{s}_{m}.csv".format(hp=hyperp,t=t,l=label,d=data,s=seq,m=m))
                        cmd = "\"runSensitivity(\'{paramfile}\', \'{outfile}\', \'{typesidx}\', \'{labelsidx}\', \'{dataidx}\', {seqsidx}, \'{methodsidx}\', {semisup},\'{hyperp}\',{hyperv});\"\n".format(
                            paramfile=paramfile,outfile=out,typesidx=t,labelsidx=label,
                            dataidx=data,seqsidx=seq,methodsidx=m,semisup=semisup,hyperp=hyperp,hyperv=hyperv)
                        cmd = boilerplate + prefix + cmd
                        job = "{d}_{t}_{l}_{s}_{m}_{hyperp}_{hyperv}.sh".format(t=t,l=label,d=data,s=seq,m=m,hyperp=hyperp,hyperv=hyperv)
                        with open(job, 'w') as fid:
                            fid.write(cmd)
                        call(["sbatch", job])

