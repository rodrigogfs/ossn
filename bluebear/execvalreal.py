import os
import subprocess
import glob
from pathlib import Path

filenames = sorted(glob.glob('../realstreams/val*')) 
labnames = sorted(glob.glob('../realstreams/lab_val*'))
methods = ['manifold', 'id', 'purecomp', 'slvq', 'purecomp_OF', 'SLVQ_OF']
for m in methods:
	for f,l in zip(filenames,labnames):
		onlyname = Path(f).stem
		str = ("#!/bin/bash\n\
#SBATCH --ntasks 1\n\
#SBATCH --time 240:0:0\n\
#SBATCH --qos bbdefault\n\
#SBATCH --mail-type END\n\
set -e\n\
cd ../onlinessl/\n\
module purge; module load bluebear\n\
module load MATLAB/2019b\n\
\n\
matlab -nodisplay -r \"randomizedSearch(\'{method}\',2000,\'{xfile}\',\'{lfile}\',\'{rfile}.mat\')\" > ~/jobs/{rfile}.txt\n\
").format(method=m,xfile=f,lfile=l,rfile='resreal'+'_'+m+'_'+onlyname)
		pbs = 'real_{}_{}.sh'.format(m,onlyname)
		with open(pbs, 'w') as fid:
			fid.write(str)
			fid.close()
		subprocess.call(["sbatch", pbs])
		#os.remove(pbs)


