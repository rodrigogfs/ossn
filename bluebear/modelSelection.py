# Model selection for MOA algorithms

#from numpy import loadtxt, mean, asarray
from os import sep
from glob import glob
from pathlib import Path
#import pandas as pd
from sys import argv

def myread(file):
    lacc = []
    with open(file, 'r') as f:
        f.readline() # header
        prev=-1
        for line in f:
            if line.strip():
                l = line.split(',')
                try:
                    if float(l[0])>prev:
                        lacc.append(float(l[4]))
                        prev = float(l[0])
                    else:
                        break
                except:
                    break    
    if len(lacc)>0:
        acc = sum(lacc) / len(lacc)
    else:
        acc = 0
    return acc

def mymean(data):
    y = data['classifications correct (percent)']
    accuracyTotal = 0
    for accuracy in y:
        accuracyTotal = accuracyTotal + float(accuracy)
    accuracy = accuracyTotal/len(y)
    return accuracy

def datacut(data):
    v=-1
    cut=-1
    for i in range(1,len(data.index)):
        if int(float(data.iloc[i,0])) > v:
            cut = i
            v=int(float(data.iloc[i,0]))
        else:
            break
    return data.iloc[0:cut,:]   

# datanamesabrupt = sorted(glob('../../ArtificialDataSets/abrupt_weka/sup_val*'))
# datanamesgradual = sorted(glob('../../ArtificialDataSets/gradual_weka/sup_val*'))
# datanamesreal = sorted(glob('../../realdatasets/realstreams_weka/sup_val*'))
# datanames = datanamesabrupt + datanamesgradual + datanamesreal
# methods = ['htnb', 'ozabag', 'oaue', 'rcd', 'ddd', 'dp']

rdir = argv[1] #'valresults' + sep + '*' # Result directory
val = sorted(glob(rdir))
params = {}

for f in val:
    onlyname = Path(f).stem
    print('**********'+onlyname+'*************')
    # print(*methods, sep = ",")
    att = onlyname.split('_') # getting name and particular params
    method = att[0]
    if att[1]=='g241d' or att[1]=='digit1':
        type='extra'
        data=att[1]
        if method=='htnb':
            thisparams=[]
        else:
            thisparams=att[2:] # there is no mention of type of selection in these streams' file names
    elif att[2]=='Sine' or att[2]=='SEA' or att[2]=='Agrawal' or att[2]=='STAGGER':
        type='art'
        data=att[2]
        if method=='htnb':
            thisparams=[]
        else:
            thisparams=att[3:]
    elif att[2]=='outdoor-classes-0-19' or att[2]=='rialto-classes-0-4':
        type='visual'
        data=att[2]
        if method=='htnb':
            thisparams=[]
        else:
            thisparams=att[3:]
    else:
        type='real'
        data=att[2]
        if method=='htnb':
            thisparams=[]
        else:
            thisparams=att[3:]
    if type not in params.keys():
        params[type] = {}
        bestacc = 0
        bestparam = ''
    if data not in params[type].keys():
        params[type][data] = {}
        bestacc = 0
        bestparam = ''
    if method not in params[type][data].keys():
        params[type][data][method] = {}
        bestacc = 0
        bestparam = ''
    
    acc = myread(f)
    
    '''
    try:
        table = pd.read_csv(f, error_bad_lines=False)
        y = table['classifications correct (percent)']
        acc = pd.DataFrame.mean(y)
    except:
        continue
    '''
    #table = datacut(table)
    #acc = mymean(table)
    #except:
    #    exit()
    #    continue
    '''
    if len(data.shape) > 1:
        acc = mean(data, axis=0) # accuracy is the 5th column in MOA files          
        acc = acc[4]
    else:
        acc = data[4]
    '''
    
    if acc > bestacc:
        thisp = {}
        for p in thisparams:
            if p[1]=='-':
                thisp[p[0]] = p[2:] # algs that are parameters are divided by '-'
            else:
                thisp[p[0]] = p[1:]
        bestparam = thisp
        bestacc = acc
    params[type][data][method] = bestparam
print(params)
