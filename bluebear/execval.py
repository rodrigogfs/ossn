import os
import subprocess
import glob
from pathlib import Path

filenamesabrupt = sorted(glob.glob('../abrupt_matlab/val*')) 
labnamesabrupt = sorted(glob.glob('../abrupt_matlab/lab_val*'))
filenamesgradual = sorted(glob.glob('../gradual_matlab/val*')) 
labnamesgradual = sorted(glob.glob('../gradual_matlab/lab_val*'))
filenames = filenamesabrupt + filenamesgradual
labnames = labnamesabrupt + labnamesgradual
methods = ['manifold', 'id', 'purecomp', 'slvq', 'purecomp_OF', 'SLVQ_OF']
for m in methods:
	for f,l in zip(filenames,labnames):	
		onlyname = Path(f).stem
		str = ("#!/bin/bash\n\
#SBATCH --ntasks 1\n\
#SBATCH --time 240:0:0\n\
#SBATCH --qos bbdefault\n\
#SBATCH --mail-type END\n\
set -e\n\
cd ../onlinessl/\n\
module purge; module load bluebear\n\
module load MATLAB/2019b\n\
\n\
matlab -nodisplay -r \"randomizedSearch(\'{method}\',2000,\'{xfile}\',\'{lfile}\',\'{rfile}.mat\')\" > ~/jobs/{rfile}.txt\n\
").format(method=m,xfile=f,lfile=l,rfile='res'+'_'+m+'_'+onlyname)
		pbs = 'ossl_{}_{}.sh'.format(m,onlyname)
		with open(pbs, 'w') as fid:
			fid.write(str)
			fid.close()
		subprocess.call(["sbatch", pbs])
		#os.remove(pbs)


