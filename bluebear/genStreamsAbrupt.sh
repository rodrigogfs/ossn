#!/bin/bash
BASEDIR="abrupt_orig"
MOAJAR="../moa/lib/moa.jar"
# First 4 streams are completely random streams with balanced classes for parameter tuning
# Starting from the 5th stream, if you change all the "-w 1" to "-w 2000", it comes a gradual drift stream.

# Preliminary parameter tunning data stream - Sine
java -cp $MOAJAR moa.DoTask \
"WriteStreamToARFFFile -s 
    (ConceptDriftStream -s (generators.SineGenerator -f 4 -b)  
        -d (ConceptDriftStream -s (generators.SineGenerator -f 3 -b) 
            -d (ConceptDriftStream -s (generators.SineGenerator -f 1 -b) 
                -d (ConceptDriftStream -s (generators.SineGenerator -f 2 -b) 
                    -d (ConceptDriftStream -s (generators.SineGenerator -f 1 -b) -d (generators.SineGenerator -f 3 -b) -p 4000 -w 1044)
                -p 4000 -w 2629)
            -p 4000 -w 654)
        -p 4000 -w 2245)
    -p 4000 -w 852)
-f $BASEDIR/val_Sine.arff -m 24000"

# Preliminary parameter tunning data stream - Agrawal
java -cp $MOAJAR moa.DoTask \
"WriteStreamToARFFFile -s 
    (ConceptDriftStream -s (generators.AgrawalGenerator -f 3 -b)  
        -d (ConceptDriftStream -s (generators.AgrawalGenerator -f 9 -b) 
            -d (ConceptDriftStream -s (generators.AgrawalGenerator -f 4 -b) 
                -d (ConceptDriftStream -s (generators.AgrawalGenerator -f 7 -b) 
                    -d (ConceptDriftStream -s (generators.AgrawalGenerator -f 6 -b) -d (generators.AgrawalGenerator -f 4 -b) -p 4000 -w 300)
                -p 4000 -w 140)
            -p 4000 -w 380)
        -p 4000 -w 200)
    -p 4000 -w 500)
-f $BASEDIR/val_Agrawal.arff -m 24000"

# Preliminary parameter tunning data stream - SEA
java -cp $MOAJAR moa.DoTask \
"WriteStreamToARFFFile -s 
    (ConceptDriftStream -s (generators.SEAGenerator -f 4 -b)  
        -d (ConceptDriftStream -s (generators.SEAGenerator -f 4 -b) 
            -d (ConceptDriftStream -s (generators.SEAGenerator -f 3 -b) 
                -d (ConceptDriftStream -s (generators.SEAGenerator -f 1 -b) 
                    -d (ConceptDriftStream -s (generators.SEAGenerator -f 4 -b) -d (generators.SEAGenerator -f 1 -b) -p 4000 -w 266)
                -p 4000 -w 172)
            -p 4000 -w 480)
        -p 4000 -w 540)
    -p 4000 -w 280)
-f $BASEDIR/val_SEA.arff -m 24000"

# Preliminary parameter tunning data stream - STAGGER
java -cp $MOAJAR moa.DoTask \
"WriteStreamToARFFFile -s 
    (ConceptDriftStream -s (generators.STAGGERGenerator -f 1 -b)  
        -d (ConceptDriftStream -s (generators.STAGGERGenerator -f 2 -b) 
            -d (ConceptDriftStream -s (generators.STAGGERGenerator -f 3 -b) 
                -d (ConceptDriftStream -s (generators.STAGGERGenerator -f 2 -b) 
                    -d (ConceptDriftStream -s (generators.STAGGERGenerator -f 1 -b) -d (generators.STAGGERGenerator -f 3 -b) -p 4000 -w 416)
                -p 4000 -w 188)
            -p 4000 -w 104)
        -p 4000 -w 11)
    -p 4000 -w 250)
-f $BASEDIR/val_STAGGER.arff -m 24000"

#=========================================Sine======================================

# Stream 1
java -cp $MOAJAR moa.DoTask \
"WriteStreamToARFFFile -s 
    (ConceptDriftStream -s (generators.SineGenerator -f 3 -b)  
        -d (ConceptDriftStream -s (generators.SineGenerator -f 4 -b) -d (generators.SineGenerator -f 3 -b) -p 4000 -w 1)
    -p 4000 -w 1) 
-f $BASEDIR/stream1.arff -m 12000"

# Stream 2
java -cp $MOAJAR moa.DoTask \
"WriteStreamToARFFFile -s 
    (ConceptDriftStream -s (generators.SineGenerator -f 1 -b)  
        -d (ConceptDriftStream -s (generators.SineGenerator -f 2 -b) 
            -d (ConceptDriftStream -s (generators.SineGenerator -f 3 -b) 
                -d (ConceptDriftStream -s (generators.SineGenerator -f 4 -b) -d (generators.SineGenerator -f 1 -b) -p 4000 -w 1)
            -p 4000 -w 1)
        -p 4000 -w 1)
    -p 4000 -w 1) 
-f $BASEDIR/stream2.arff -m 20000"

#=========================================Agrawal======================================

# Stream 3
java -cp $MOAJAR moa.DoTask \
"WriteStreamToARFFFile -s 
    (ConceptDriftStream -s (generators.AgrawalGenerator -f 1 -b)  
        -d (ConceptDriftStream -s (generators.AgrawalGenerator -f 3 -b) 
            -d (ConceptDriftStream -s (generators.AgrawalGenerator -f 4 -b) 
                -d (ConceptDriftStream -s (generators.AgrawalGenerator -f 7 -b) -d (generators.AgrawalGenerator -f 10 -b) -p 4000 -w 1)
            -p 4000 -w 1)
        -p 4000 -w 1)
    -p 4000 -w 1) 
-f $BASEDIR/stream3.arff -m 20000"

# Stream 4
java -cp $MOAJAR moa.DoTask \
"WriteStreamToARFFFile -s 
    (ConceptDriftStream -s (generators.AgrawalGenerator -f 7 -b)  
        -d (ConceptDriftStream -s (generators.AgrawalGenerator -f 4 -b) 
            -d (ConceptDriftStream -s (generators.AgrawalGenerator -f 6 -b) 
                -d (ConceptDriftStream -s (generators.AgrawalGenerator -f 5 -b) 
                    -d (ConceptDriftStream -s (generators.AgrawalGenerator -f 2 -b) -d (generators.AgrawalGenerator -f 9 -b) -p 4000 -w 1)
                -p 4000 -w 1)
            -p 4000 -w 1)
        -p 4000 -w 1)
    -p 4000 -w 1)
-f $BASEDIR/stream4.arff -m 24000"

# Stream 5
java -cp $MOAJAR moa.DoTask \
"WriteStreamToARFFFile -s 
    (ConceptDriftStream -s (generators.AgrawalGenerator -f 4 -b)  
        -d (ConceptDriftStream -s (generators.AgrawalGenerator -f 2 -b) 
            -d (ConceptDriftStream -s (generators.AgrawalGenerator -f 1 -b) 
                -d (ConceptDriftStream -s (generators.AgrawalGenerator -f 3 -b) -d (generators.AgrawalGenerator -f 4 -b) -p 4000 -w 1)
            -p 4000 -w 1)
        -p 4000 -w 1)
    -p 4000 -w 1) 
-f $BASEDIR/stream5.arff -m 20000"

# Stream 6
java -cp $MOAJAR moa.DoTask \
"WriteStreamToARFFFile -s 
    (ConceptDriftStream -s (generators.AgrawalGenerator -f 1 -b)  
        -d (ConceptDriftStream -s (generators.AgrawalGenerator -f 3 -b) 
            -d (ConceptDriftStream -s (generators.AgrawalGenerator -f 6 -b) 
                -d (ConceptDriftStream -s (generators.AgrawalGenerator -f 5 -b) -d (generators.AgrawalGenerator -f 4 -b) -p 4000 -w 1)
            -p 4000 -w 1)
        -p 4000 -w 1)
    -p 4000 -w 1) 
-f $BASEDIR/stream6.arff -m 20000"

#=========================================SEA======================================

# Stream 7
java -cp $MOAJAR moa.DoTask \
"WriteStreamToARFFFile -s 
    (ConceptDriftStream -s (generators.SEAGenerator -f 4 -b)  
        -d (ConceptDriftStream -s (generators.SEAGenerator -f 3 -b) 
            -d (ConceptDriftStream -s (generators.SEAGenerator -f 1 -b) 
                -d (ConceptDriftStream -s (generators.SEAGenerator -f 2 -b) -d (generators.SEAGenerator -f 4 -b) -p 4000 -w 1)
            -p 4000 -w 1)
        -p 4000 -w 1)
    -p 4000 -w 1) 
-f $BASEDIR/stream7.arff -m 20000"

# Stream 8
java -cp $MOAJAR moa.DoTask \
"WriteStreamToARFFFile -s 
    (ConceptDriftStream -s (generators.SEAGenerator -f 4 -b)  
        -d (ConceptDriftStream -s (generators.SEAGenerator -f 1 -b) 
            -d (ConceptDriftStream -s (generators.SEAGenerator -f 4 -b) 
                -d (ConceptDriftStream -s (generators.SEAGenerator -f 3 -b) -d (generators.SEAGenerator -f 2 -b) -p 4000 -w 1)
            -p 4000 -w 1)
        -p 4000 -w 1)
    -p 4000 -w 1) 
-f $BASEDIR/stream8.arff -m 20000"

#=========================================STAGGER======================================

# Stream 9
java -cp $MOAJAR moa.DoTask \
"WriteStreamToARFFFile -s 
    (ConceptDriftStream -s (generators.STAGGERGenerator -b)
        -d (ConceptDriftStream -s (generators.STAGGERGenerator -f 2 -b)
            -d (ConceptDriftStream -s (generators.STAGGERGenerator -f 3 -b) -d (generators.STAGGERGenerator -f 2 -b) -p 4000 -w 1)
        -p 4000 -w 1)
    -p 4000 -w 1) 
-f $BASEDIR/stream9.arff -m 16000"

# Stream 10
java -cp $MOAJAR moa.DoTask \
"WriteStreamToARFFFile -s 
    (ConceptDriftStream -s (generators.STAGGERGenerator -f 2 -b)
        -d (ConceptDriftStream -s (generators.STAGGERGenerator -f 3 -b)
            -d (ConceptDriftStream -s (generators.STAGGERGenerator -f 1 -b) -d (generators.STAGGERGenerator -f 2 -b) -p 4000 -w 1)
        -p 4000 -w 1)
    -p 4000 -w 1) 
-f $BASEDIR/stream10.arff -m 16000"
