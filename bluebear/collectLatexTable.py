from sys import argv
from os.path import join, isfile
import numpy as np

tempdir = argv[1] #'../../prog/temp/'
filecsv = argv[2] #'C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\finalresults_OSSL_SGD_Batch.csv' # Result file
filetex = argv[3] #'C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\finalresults_OSSL_SGD_Batch_latex.tex' # Result file
rcsv = open(filecsv, 'w', buffering=1)
rtex = open(filetex, 'w', buffering=1)
streams = {'abrupt':{'Sine':[1,2],'Agrawal':[3,4,5,6],'SEA':[7,8],'STAGGER':[9,10]},
           'gradual':{'Sine':[1,2],'Agrawal':[3,4,5,6],'SEA':[7,8],'STAGGER':[9,10]},
           'real':{'elec2':[1],'noaa':[1],'powersupplyDayNight':[1],'sensorClasses29and31':[1]},
           'extra':{'g241d':[1],'digit1':[1]}
           }
labelsrate = ['5', '10', '20']
labelsextra = ['10', '100']
#methods = ['manifold', 'purecomp', 'slvq', 'purecomp_OF', 'SLVQ_OF', 'id']
methods = ['manifold']
rcsv.write('Methods {}\n'.format(' '.join(methods)))
for t,s in streams.items():
    rtex.write('\\multicolumn{{13}}{{c}}{{Type of drift: {}}} \\\\ \n'.format(t))
    rcsv.write('Drift: {} \n'.format(t))
    if t=='extra':
        labels = labelsextra
    else:
        labels = labelsrate
    for label in labels:
        rtex.write('\\multicolumn{{13}}{{c}}{{Labels at {}}} \\\\ \n'.format(label))
        rcsv.write('Labels at {}\n'.format(label))
        rtex.write('\\multicolumn{{13}}{{c}}{{Methods {}}} \\\\ \n'.format(' '.join(methods)))
        for data,seqs in s.items():
            for seq in seqs:
                if t=='real':
                    rtex.write(data+' & ')
                    rcsv.write(data+',')
                else:
                    rtex.write(data+str(1+seq-min(seqs))+' & ')
                    rcsv.write(data+str(1+seq-min(seqs))+',')
                for m in methods:
                    rfile = join(tempdir, "{d}_{t}_{l}_{s}_{m}.csv".format(t=t,l=label,d=data,s=seq,m=m))
                    print(rfile)
                    if isfile(rfile):
                        preq = np.loadtxt(rfile)
                        acc = np.mean(preq)
                        accstd = np.std(preq, ddof=1)
                    else:
                        print('SUCH FILE DOES NOT EXIST')
                        acc = accstd = '?'
                    if m == methods[-1]:
                        rtex.write("{a}$\\pm${sd} \\\\ \n".format(a=acc,sd=accstd))
                        rcsv.write("{a},{sd}\n".format(a=acc,sd=accstd))
                    else:
                        rtex.write("{a}$\\pm${sd} & ".format(a=acc,sd=accstd))
                        rcsv.write("{a},{sd},".format(a=acc,sd=accstd))
rtex.close()
rcsv.close()