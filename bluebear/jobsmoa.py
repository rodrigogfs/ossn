import os
import subprocess
import glob
from pathlib import Path

ensemblesize = [5, 10, 15, 20]
batchsize = [200, 400, 500, 600, 800, 1000]
evalperiod = [200, 400, 500, 600, 800, 1000]
pvalue = [0.01, 0.05]
driftdetector=['DDM', 'EDDM', 'ADWINChangeDetector']
lambdaddd = [0.0005, 0.001, 0.005, 0.01, 0.05, 0.1, 0.5]
stats = ['EntropyMeasure', 'QStatistics']

prefix = "java -cp moa/lib/moa.jar -javaagent:moa/lib/sizeofag-1.0.4.jar moa.DoTask "
boilerplate = ("#!/bin/bash\n\
#SBATCH --ntasks 1\n\
#SBATCH --qos bbshort\n\
#SBATCH --mail-type NONE\n\
set -e\n\
module purge; module load bluebear\n\
module load Java/1.8.0_92\n\
\n")

rdir = 'results' + os.sep # Result directory
filenamesabrupt = sorted(glob.glob('../abrupt_matlab/sup_val*'))
filenamesgradual = sorted(glob.glob('../gradual_matlab/sup_val*'))
filenamesreal = sorted(glob.glob('../realstreams/sup_val_*'))
#filenamesabrupt = sorted(glob.glob("C:/Users/Rodrigo/Documents/programming/basesMatlab/bases5/*.arff"))
#filenamesgradual = sorted(glob.glob("C:/Users/Rodrigo/Documents/programming/basesMatlab/bases20/*.arff"))
filenames = filenamesabrupt + filenamesgradual + filenamesreal

for f in filenames:
    onlyname = Path(f).stem
    print('Doing '+onlyname)
    # HTNB
    '''
    cmd = "'EvaluatePrequential -l (trees.HoeffdingAdaptiveTree -l NB) -s (clustering.SimpleCSVStream -f {input} -c) -i -1 -f 500 -a 0.999 -d {output}'\n".format(input=f,output=rdir+'htnb_'+onlyname+'.csv')
    cmd = boilerplate + prefix + cmd + '\n'
    job = 'htnb_{}.sh'.format(onlyname)
    with open(job, 'w') as fid:
        fid.write(cmd)
    subprocess.call(["sbatch", job])
    #os.remove(job)
    
    # OzaBag Adwin
    for es in ensemblesize:
        cmd = ("'EvaluatePrequential -l (meta.OzaBagAdwin -s {s}) -s (clustering.SimpleCSVStream -f {input} -c) -f 500 -d {output} -a 0.999'\n").format(input=f,output=rdir+'ozabag_'+onlyname+'_s'+str(es)+'.csv',s=es)
        cmd = boilerplate + prefix + cmd + '\n'
        job = 'ozabag_{}_{}.sh'.format(onlyname,str(es))
        with open(job, 'w') as fid:
            fid.write(cmd)
        subprocess.call(["sbatch", job])
        #os.remove(job)
    '''
    # OAUE
    for es in ensemblesize:
        for ev in evalperiod:
            out = rdir + 'oaue_'+onlyname+'_n'+str(es)+'_w'+str(ev)+'.csv'
            cmd = ("'EvaluatePrequential -l (meta.OnlineAccuracyUpdatedEnsemble -n {esize} "
            "-w {window}) -s (clustering.SimpleCSVStream -f {input} -c) "
            "-f 1 -d {output} -a 0.999'\n").format(input=f,output=out,
                                                    esize=es,window=ev)
            cmd = boilerplate + prefix + cmd + '\n'
            job = 'oeua_{name}_n{esize}_w{window}.sh'.format(name=onlyname,esize=str(es),window=str(ev))
            with open(job, 'w') as fid:
                fid.write(cmd)
            subprocess.call(["sbatch", job])
            #os.remove(job)
    '''
    # RCD
    for es in ensemblesize:
        for ev in evalperiod:
            for pv in pvalue:
                for dd in driftdetector:
                    for batch in batchsize:
                        out = rdir + 'rcd_{name}_s{s}_b{b}_t{t}_c{c}_d-{d}.csv'.format(name=onlyname,
                                                                               s=str(int(pv*100)),b=str(batch),
                                                                               t=str(ev),c=str(es),d=dd)
                        cmd = ("'EvaluatePrequential -l (meta.RCD -s {s} -b {b} -t {t} -c {c} -l trees.HoeffdingTree"
                               " -d {d}) -s (clustering.SimpleCSVStream -c -f {input}) "
                               "-f 500 -d {output} -a 0.999'\n").format(input=f,output=out,
                                                                      s=str(pv),b=str(batch),
                                                                      t=str(ev),c=str(es),d=dd)
                        cmd = boilerplate + prefix + cmd + '\n'
                        job = 'rcd_{name}_s{s}_b{b}_t{t}_c{c}_d-{d}.sh'.format(name=onlyname,
                                                                      s=str(int(pv*100)),b=str(batch),
                                                                      t=str(ev),c=str(es),d=dd)
                        with open(job, 'w') as fid:
                            fid.write(cmd)
                        subprocess.call(["sbatch", job])
                        #os.remove(job)
    
    #DDD
    for dd in driftdetector:
        for lam in lambdaddd:
            out = rdir + 'ddd_{name}_w{w}_d-{d}.csv'.format(name=onlyname,
                                                            w=str(int(lam*10000)),
                                                            d=dd)
            cmd = ("'EvaluatePrequential -l (meta.DiversityForDealingWithDrifts -d {d} -w {w})"
                   " -s (clustering.SimpleCSVStream -f {input} -c)"
                   " -f 1 -d {output} -a 0.999'\n").format(input=f,output=out,d=dd,w=lam)
            cmd = boilerplate + prefix + cmd + '\n'
            job = 'ddd_{name}_w{w}_d-{d}.sh'.format(name=onlyname,
                                                            w=int(lam*10000),
                                                            d=dd)
            with open(job, 'w') as fid:
                fid.write(cmd)
            subprocess.call(["sbatch", job])
            #os.remove(job)
    
    # DP
    for es in ensemblesize:
        for batch in batchsize:
            for dd in driftdetector:
                for stat in stats:
                    out = rdir + 'dp_{name}_s{s}_b{b}_t-{t}_d-{d}.csv'.format(name=onlyname,
                                                                 s=es,b=batch,d=dd,t=stat)
                    cmd = ("'EvaluatePrequential -l (meta.DiversityPool -s {s} -b {b} -d {d}" 
                          " -t {t}) -s (clustering.SimpleCSVStream -f {input} -c)" 
                          " -f 500 -d {output} -a 0.999'").format(input=f,output=out,
                                                                 s=es,b=batch,d=dd,t=stat)
                    cmd = boilerplate + prefix + cmd + '\n'
                    job = 'dp_{name}_s{s}_b{b}_t-{t}_d-{d}.sh'.format(name=onlyname,
                                                                 s=es, b=batch, d=dd, t=stat)
                    with open(job, 'w') as fid:
                        fid.write(cmd)
                    subprocess.call(["sbatch", job])
                    #os.remove(job)
    '''

