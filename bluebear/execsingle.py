import os
import subprocess
from random import seed, random, randint, expovariate
from time import time
from os.path import join

prefix = "matlab -nodisplay -nojvm -r "
boilerplate = ("#!/bin/bash\n"
"#SBATCH --ntasks 1\n"
"#SBATCH --time 240:0:0\n"
"#SBATCH --mem 8G\n"
"#SBATCH --qos bbdefault\n"
"#SBATCH --mail-type NONE\n"
"set -e\n"
"cd /rds/homes/m/minkull/spdisc-data/rodrigo/onlinessl\n"
"module purge; module load bluebear\n"
"module load MATLAB/2019b\n"
"\n")

basedir = '/rds/homes/m/minkull/spdisc-data/rodrigo/data'
rdir = '/rds/homes/m/minkull/spdisc-data/rodrigo/onlinessl/val' # Result directory

'''
streams = {'abrupt':{'Sine':[1,2],'Agrawal':[3,4,5,6],'SEA':[7,8],'STAGGER':[9,10]},
           'gradual':{'Sine':[1,2],'Agrawal':[3,4,5,6],'SEA':[7,8],'STAGGER':[9,10]},
           'real':{'elec2':[1],'noaa':[1],'powersupplyDayNight':[1],'sensorClasses29and31':[1],'weatherAUSprep':[1]}
           }
'''

# active streams for this set of experiments
# streams = {'art':{'Sine':[1,2],'Agrawal':[3,4,5,6],'SEA':[7,8],'STAGGER':[9,10]},
           # 'real':{'elec2':[1],'noaa':[1],'powersupplyDayNight':[1],'sensorClasses29and31':[1]},
           # 'extra':{'g241d':[1],'digit1':[1]},
           # 'visual':{'outdoor-classes-0-19':[1],'rialto-classes-0-4',[1]}
           # }
streams = {
           'visual':{'outdoor-classes-0-19':[1],'rialto-classes-0-4':[1]}
           }

#methods = ['manifold', 'id', 'purecomp', 'slvq', 'purecomp_OF', 'SLVQ_OF']
methods = ['manifold']
#minmethods = ['SGD', 'NR', 'NAG']
jobseed = int(time())
seed(jobseed)
budget = 500
count = 0

for type, s in streams.items():
    for data in s:
        print('***************Doing '+data+' with seed '+str(jobseed)+'******************************')
        if type=='real':
            search = join(basedir, 'realstreams/val_{}.csv'.format(data))
            searchlab = join(basedir, 'realstreams/lab_val_{}_uni_l10.csv'.format(data))
        elif type=='art':
            search = join(basedir, 'abrupt_matlab/val_{}.csv'.format(data))
            searchlab = join(basedir, 'abrupt_matlab/lab_val_{}_uni_l10.csv'.format(data))
        elif type=='extra':
            search = join(basedir, 'extra/extra_matlab/{}.csv'.format(data))
            searchlab = join(basedir, 'extra/extra_matlab/lab_val_{}_l100.csv'.format(data))
        elif type=='visual':
            search = join(basedir, 'visual/val_{}.csv'.format(data))
            searchlab = join(basedir, 'visual/lab_val_{}_uni_l10.csv'.format(data))
        for m in methods:
            for i in range(budget):
                optmethod = 'NR'
                semisup = 1
                sigmarate = expovariate(5) #random() #expovariate(1) #random() #expovariate(5) # expovariate(1)
                sigmamanif = expovariate(0.5) #random()*5 #expovariate(1) #expovariate(0.25) #expovariate(0.125) #expovariate(0.25)
                eta = random() #random()*10 #expovariate(5) # random() + 1e-8
                etaw = expovariate(5)
                beta = expovariate(5)
                alpha = random() #expovariate(5) #random() + 1e-8
                epochs = 1 # randint(1,200)
                lam = random()
                width = expovariate(1) #random() + 1e-8 #expovariate(0.5) #expovariate(1) #expovariate(0.25)
                nhidden = randint(1,1000) #randint(1,200) #randint(1,500) # randint(1,200)
                batchsize = randint(1,1000) #randint(1,200) #randint(1,200) #randint(1,200) # randint(1,500)
                thisseed = jobseed + count
                count = count + 1
                out = join(rdir, "{type}_{data}_{method}_{seed}.mat".format(method=m,type=type,data=data,seed=thisseed))
                cmd = ("\"runOnlineSSL(\'{xfile}\',\'{lfile}\',{semisup},[],\'{rfile}\',"
                "\'{method}\',\'{methodw}\',{sigmarate},{sigmamanif},{eta},{etaw},"
                "{beta},{alpha},{epochs},{lam},{width},"
                "{nhidden},{batchsize},{thisseed});\" \n").format(xfile=search,lfile=searchlab,semisup=semisup,
                                            rfile=out,method=m,methodw=optmethod,sigmarate=sigmarate,
                                            sigmamanif=sigmamanif,eta=eta,etaw=etaw,beta=beta,
                                            alpha=alpha,epochs=epochs,lam=lam,
                                            width=width,nhidden=nhidden,batchsize=batchsize,
                                            thisseed=thisseed)
                job = "{type}_{data}_{method}_{seed}.sh".format(method=m,type=type,data=data,seed=thisseed)
                cmd = boilerplate + prefix + cmd + '\n'
                with open(job, 'w') as fid:
                    fid.write(cmd)
                subprocess.call(["sbatch", job])
                #os.remove(job)
print('Submitted '+str(count)+' jobs')
