#Randomized Search for MOA algorithms

from os.path import isfile, join
import subprocess
from glob import glob
from pathlib import Path
from random import random, randint, choice, seed
from time import time

thisseed = int(time())
seed(thisseed)
driftdetector=['DDM', 'EDDM', 'ADWINChangeDetector']
stats = ['EntropyMeasure', 'QStatistics']

prefix = "java -cp moa/lib/moa.jar -javaagent:moa/lib/sizeofag-1.0.4.jar moa.DoTask "
boilerplate = ("#!/bin/bash\n\
#SBATCH --ntasks 1\n\
#SBATCH --time 240:0:0\n\
#SBATCH --mem 16G\n\
#SBATCH --qos bbdefault\n\
#SBATCH --mail-type NONE\n\
set -e\n\
module purge; module load bluebear\n\
module load Java/1.8.0_92\n\
\n")

rdir = 'results' # Result directory
filenamesart = sorted(glob('../data/abrupt_matlab/val_*.arff'))
labnamesart = sorted(glob('../data/abrupt_matlab/lab_val_*.csv'))
filenamesreal = sorted(glob('../data/realstreams/val_*.arff'))
labnamesreal = sorted(glob('../data/realstreams/lab_val_*.csv'))
filenamesextra = sorted(glob('../data/extra/extra_matlab/*.arff'))
labnamesextra = sorted(glob('../data/extra/extra_matlab/lab_val_*.csv'))
filenames = filenamesart + filenamesreal + filenamesextra
labnames = labnamesart + labnamesreal + labnamesextra
budget = 84

for f, lab in zip(filenames, labnames):
    onlyname = Path(f).stem
    print('Doing '+onlyname)
    
    # HTNB
    job = 'htnb_{}.sh'.format(onlyname)
    if not isfile(job):
        out = join(rdir,'htnb_'+onlyname+'.csv')
        cmd = ("'EvaluatePrequential -l (trees.HoeffdingTree -l NB) -s (ArffFileStream -f {input}) "
        "-e  FadingFactorClassificationPerformanceEvaluator "
        "-f 1 -d {output} -z {lab}'\n").format(input=f,output=out,lab=lab)
        cmd = boilerplate + prefix + cmd + '\n'
        with open(job, 'w') as fid:
            fid.write(cmd)
        subprocess.call(["sbatch", job])
        #os.remove(job)
    
    # OzaBag
    for i in range(budget):
        es = randint(5,20)
        job = 'ozabag_{}_s{}.sh'.format(onlyname,str(es))
        if not isfile(job):
            out = join(rdir, 'ozabag_'+onlyname+'_s'+str(es)+'.csv')
            cmd = ("'EvaluatePrequential -l (meta.OzaBag -s {s}  "
                    "-l (trees.HoeffdingTree -l NB)) -s (ArffFileStream -f {input}) "
                    "-e FadingFactorClassificationPerformanceEvaluator "
                    "-f 1 -d {output} -z {lab}'\n").format(input=f,output=out,s=es,lab=lab)
            cmd = boilerplate + prefix + cmd + '\n'
            with open(job, 'w') as fid:
                fid.write(cmd)
            subprocess.call(["sbatch", job])
            #os.remove(job)
    
    # OAUE
    for i in range(budget):
        es = randint(5,20)
        ev = randint(200,1000)
        job = 'oaue_{name}_n{esize}_w{window}.sh'.format(name=onlyname,esize=str(es),window=str(ev))
        if not isfile(job):
            out = join(rdir, 'oaue_'+onlyname+'_n'+str(es)+'_w'+str(ev)+'.csv')
            cmd = ("'EvaluatePrequential -l (meta.OnlineAccuracyUpdatedEnsemble -n {esize} "
            "-w {window} -l (trees.HoeffdingTree -l NB)) -s (ArffFileStream -f {input}) "
            "-e FadingFactorClassificationPerformanceEvaluator "
            "-f 1 -d {output} -z {lab}'\n").format(input=f,output=out,
                                    esize=es,window=ev,lab=lab)
            cmd = boilerplate + prefix + cmd + '\n'
            with open(job, 'w') as fid:
                fid.write(cmd)
            subprocess.call(["sbatch", job])
            #os.remove(job)
    
    # RCD
    for i in range(budget):
        es = randint(5,20)
        ev = randint(200,1000)
        pv = random()*0.04 + 0.01
        dd = choice(driftdetector)
        batch = randint(200,1000)
        job = 'rcd_{name}_s{s}_b{b}_t{t}_c{c}_d-{d}.sh'.format(name=onlyname,
                                                           s=int(pv*1e6),b=batch,
                                                           t=ev,c=es,d=dd)
        if not isfile(job):
            out = join(rdir, 'rcd_{name}_s{s}_b{b}_t{t}_c{c}_d-{d}.csv'.format(name=onlyname,
                                                                s=int(pv*1e6),b=batch,
                                                                t=ev,c=es,d=dd))
            cmd = ("'EvaluatePrequential -l (meta.RCD -s {s} -b {b} -t {t} -c {c} -l (trees.HoeffdingTree -l NB) "
                   "-d {d}) -s (ArffFileStream -f {input}) -e FadingFactorClassificationPerformanceEvaluator "
                   "-f 1 -d {output} -z {lab}'\n").format(input=f,output=out,
                                        s=pv,b=batch,t=ev,c=es,d=dd,lab=lab)
            cmd = boilerplate + prefix + cmd + '\n'
            with open(job, 'w') as fid:
                fid.write(cmd)
            subprocess.call(["sbatch", job])
            #os.remove(job)
    
    #DDD
    for i in range(budget):
        dd = choice(driftdetector)
        es = randint(5,20)
        lam = random()*(5e-1 - 5e-4) + 5e-4
        p = random()
        h = random()
        job = 'ddd_{name}_w{w}_p{p}_h{h}_s{s}_d-{d}.sh'.format(name=onlyname,
                                                        w=int(lam*1e6),p=int(p*1e6),
                                                        h=int(h*1e6),s=es,
                                                        d=dd)
        if not isfile(job):
            out = join(rdir, 'ddd_{name}_w{w}_p{p}_h{h}_s{s}_d-{d}.csv'.format(name=onlyname,
                                                        w=int(lam*1e6),p=int(p*1e6),
                                                        h=int(h*1e6),s=es,
                                                        d=dd))
            cmd = ("'EvaluatePrequential -l (meta.DiversityForDealingWithDrifts -d {d} -w {w} -p {p} -h {h} "
                   "-l (meta.OzaBagDDD -l (trees.HoeffdingTree -l NB) -s {s})) "
                   "-s (ArffFileStream -f {input}) -e FadingFactorClassificationPerformanceEvaluator "
                   "-f 1 -d {output} -z {lab}'\n").format(input=f,output=out,d=dd,w=lam,p=p,h=h,s=es,lab=lab)
            cmd = boilerplate + prefix + cmd + '\n'
            with open(job, 'w') as fid:
                fid.write(cmd)
            subprocess.call(["sbatch", job])
            #os.remove(job)
    
    # DP
    for i in range(budget):
        es = randint(5,20)
        dd = choice(driftdetector)
        batch = randint(200,1000)
        stat = choice(stats)
        job = 'dp_{name}_s{s}_b{b}_t-{t}_d-{d}.sh'.format(name=onlyname,
                                                      s=es,b=batch,d=dd,t=stat)
        if not isfile(job):
            out = join(rdir, 'dp_{name}_s{s}_b{b}_t-{t}_d-{d}.csv'.format(name=onlyname,
                                           s=es,b=batch,d=dd,t=stat))
            cmd = ("'EvaluatePrequential -l (meta.DiversityPool -s {s} -b {b} -d {d} " 
                    "-t {t} -l (trees.HoeffdingTree -l NB)) -s (ArffFileStream -f {input}) "
                    "-e FadingFactorClassificationPerformanceEvaluator "
                    "-f 1 -d {output} -z {lab}'").format(input=f,output=out,
                                        s=es,b=batch,d=dd,t=stat,lab=lab)
            cmd = boilerplate + prefix + cmd + '\n'
            with open(job, 'w') as fid:
                fid.write(cmd)
            subprocess.call(["sbatch", job])
            #os.remove(job)
