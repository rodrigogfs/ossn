from numpy import loadtxt
from glob import glob
from sys import argv
from pathlib import Path
from os.path import join

infiles = glob(argv[1])
for file in infiles:
    # reading file twice this is needed to calculate number of rows
    m = loadtxt(file, delimiter=',') 
    numrows = m.shape[1] # numpy calculates number of rows
    with open(file, 'r') as fin:
        data = fin.read()
    name = Path(file).stem
    header = '@RELATION {}\n\n'.format(name)
    a = ''
    for j in range(numrows-1):
        a += '@ATTRIBUTE a{} numeric\n'.format(j)
    a += '@ATTRIBUTE class {{0,1}}\n\n'.format(j)
    header += a + '@DATA\n'
    out = join(argv[2], name+'.arff')
    print(out)	
    with open(out, 'w') as fout:
        fout.write(header+data)
    #savetxt(argv[2]+name+'.arff', m, delimiter=',', header=header, comments='', fmt='%10.5f')
