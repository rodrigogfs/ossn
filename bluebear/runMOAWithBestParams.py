#Run MOA algorithms with params found by Randomized Search

from subprocess import call
from os import remove
from glob import glob
from pathlib import Path
from time import time
import pandas as pd
import numpy as np
from os.path import join

def myread(file):
    with open(file, 'r') as f:
        f.readline() # header
        lacc = []
        prev=-1
        for line in f:
            if line.strip():
                l = line.split(',')
                try:
                    if float(l[0])>prev:
                        lacc.append(float(l[4]))
                        prev = float(l[0])
                    else:
                        break
                except:
                    break
    a = np.asarray(lacc)
    acc = np.mean(a)
    accstd = np.std(a, ddof=1)
    return acc, accstd, a

def writefile(cmd, out, rcsv, rtex, vecdir, last):
    print('lendo ' + out)
    print('COMANDO: '+cmd)
    call(cmd, shell=True)
    acc, accstd, vec = myread(out)
    if last:
        rcsv.write("{:.3f},{:.3f}\n".format(acc, accstd))
        rtex.write("{:.3f}$\\pm${:.3f} \\\\ \n".format(acc, accstd))
    else:
        rcsv.write("{:.3f},{:.3f},".format(acc,accstd))
        rtex.write("{:.3f}$\\pm${:.3f} & ".format(acc,accstd))
    np.savetxt(join(vecdir, Path(out).stem+'_vec.csv'), vec)
    remove(out)    
    '''
    # OLD CODE WITH PANDAS
    # try:
        # data = pd.read_csv(out) #pd.read_csv(out, error_bad_lines=False)
        # y = data['classifications correct (percent)']
        # acc = pd.DataFrame.mean(y)
        # accstd =  pd.DataFrame.std(y)
    # except:
        # acc, accstd = myread(out)
    '''       
    # skip 2 rows because the 1st is the header and the 2nd has '?' character
    # data = loadtxt(out, delimiter=',', skiprows=5) 
    #if len(data.shape) > 1:
    #    acc = mean(data[:,4]) # accuracy is the 5th column in MOA files
    #    accstd = std(data[:,4], ddof=1)
    #else:
    #    acc = data[4]
    #    accstd = 0
    #except:
    #    acc=0
    #    accstd=0

prefix = "java -cp ../../prog/moa/lib/moa.jar -javaagent:../../prog/moa/lib/sizeofag-1.0.4.jar moa.DoTask "
tempdir = '../../prog/others/tempresults/' # Temp Result directory
vecdir = 'moavectors' # directory for prequential accuracy vectors
filecsv = '../../prog/others/MOA_neweval_uni.csv' # Result file
filetex = '../../prog/others/MOA_neweval_uni.tex' # Result file
rcsv = open(filecsv, 'w', buffering=1)
rtex = open(filetex, 'w', buffering=1)
'''
params = {
'abrupt': {'Sine':{'htnb':None,'ozabag':{'s':6},'rcd':{'s':0.03,'b':972,'t':780,'c':11,'d':'EDDM'},'ddd':{'w':0.2167,'d':'DDM'},'dp':{'s':10,'b':433,'t':'EntropyMeasure','d':'EDDM'}},
           'Agrawal':{'htnb':None,'ozabag':{'s':11},'rcd':{'s':0.01,'b':297,'t':857,'c':18,'d':'DDM'},'ddd':{'w':0.2910,'d':'ADWINChangeDetector'},'dp':{'s':5,'b':998,'t':'EntropyMeasure','d':'EDDM'}},
           'SEA':{'htnb':None,'ozabag':{'s':18},'rcd':{'s':0.03,'b':535,'t':881,'c':15,'d':'DDM'},'ddd':{'w':0.3486,'d':'DDM'},'dp':{'s':7,'b':305,'t':'QStatistics','d':'DDM'}},
           'STAGGER':{'htnb':None,'ozabag':{'s':14},'rcd':{'s':0.03,'b':839,'t':595,'c':10,'d':'DDM'},'ddd':{'w':0.4042,'d':'DDM'},'dp':{'s':7,'b':519,'t':'EntropyMeasure','d':'EDDM'}}
          },
'gradual': {'Sine':{'htnb':None,'ozabag':{'s':13},'rcd':{'s':0.04,'b':324,'t':967,'c':18,'d':'DDM'},'ddd':{'w':0.1824,'d':'DDM'},'dp':{'s':13,'b':780,'t':'EntropyMeasure','d':'DDM'}},
           'Agrawal':{'htnb':None,'ozabag':{'s':12},'rcd':{'s':0.02,'b':805,'t':474,'c':17,'d':'EDDM'},'ddd':{'w':0.548,'d':'ADWINChangeDetector'},'dp':{'s':8,'b':589,'t':'QStatistics','d':'EDDM'}},
           'SEA':{'htnb':None,'ozabag':{'s':13},'rcd':{'s':0.01,'b':539,'t':666,'c':6,'d':'EDDM'},'ddd':{'w':0.364,'d':'ADWINChangeDetector'},'dp':{'s':11,'b':372,'t':'EntropyMeasure','d':'ADWINChangeDetector'}},
           'STAGGER':{'htnb':None,'ozabag':{'s':10},'rcd':{'s':0.01,'b':442,'t':892,'c':11,'d':'DDM'},'ddd':{'w':0.4527,'d':'DDM'},'dp':{'s':6,'b':222,'t':'QStatistics','d':'DDM'}}
          },
'real':    {'elec2':{'htnb':None,'ozabag':{'s':5},'rcd':{'s':0.01,'b':436,'t':992,'c':11,'d':'DDM'},'ddd':{'w':0.4531,'d':'ADWINChangeDetector'},'dp':{'s':5,'b':542,'t':'EntropyMeasure','d':'DDM'}},
           'noaa':{'htnb':None,'ozabag':{'s':20},'rcd':{'s':0.01,'b':757,'t':486,'c':11,'d':'EDDM'},'ddd':{'w':0.131,'d':'ADWINChangeDetector'},'dp':{'s':16,'b':651,'t':'EntropyMeasure','d':'DDM'}},
           'powersupplyDayNight':{'htnb':None,'ozabag':{'s':20},'rcd':{'s':0.01,'b':245,'t':484,'c':16,'d':'DDM'},'ddd':{'w':0.2611,'d':'DDM'},'dp':{'s':10,'b':407,'t':'QStatistics','d':'ADWINChangeDetector'}},
           'sensorClasses29and31':{'htnb':None,'ozabag':{'s':20},'rcd':{'s':0.03,'b':631,'t':938,'c':20,'d':'DDM'},'ddd':{'w':0.1076,'d':'DDM'},'dp':{'s':11,'b':207,'t':'QStatistics','d':'EDDM'}},
           'weatherAUS_prep':{'htnb':None,'ozabag':{'s':14},'rcd':{'s':0.01,'b':709,'t':263,'c':17,'d':'ADWINChangeDetector'},'ddd':{'w':0.1927,'d':'ADWINChangeDetector'},'dp':{'s':19,'b':444,'t':'EntropyMeasure','d':'DDM'}}
          }
}
'''
'''
paramsBIGARTSTREAMS = {'abrupt': {'Agrawal': {'ddd': {'w': '357', 'd': 'EDDM'}, 'dp': {'s': '5', 'b': '867', 't': 'EntropyMeasure', 'd': 'EDDM'}, 'htnb': {}, 'oaue': {'n': '12', 'w': '215'}, 'ozabag': {'s': '15'}, 'rcd': {'s': '2', 'b': '359', 't': '687', 'c': '15', 'd': 'DDM'}}, 'SEA': {'ddd': {'w': '103', 'd': 'EDDM'}, 'dp': {'s': '10', 'b': '584', 't': 'QStatistics', 'd': 'DDM'}, 'htnb': {}, 'oaue': {'n': '20', 'w': '209'}, 'ozabag': {'s': '16'}, 'rcd': {'s': '1', 'b': '709', 't': '956', 'c': '7', 'd': 'DDM'}}, 'STAGGER': {'ddd': {'w': '121', 'd': 'EDDM'}, 'dp': {'s': '10', 'b': '683', 't': 'QStatistics', 'd': 'EDDM'}, 'htnb': {}, 'oaue': {'n': '5', 'w': '219'}, 'ozabag': {'s': '6'}, 'rcd': {'s': '3', 'b': '918', 't': '992', 'c': '5', 'd': 'DDM'}}, 'Sine': {'ddd': {'w': '1043', 'd': 'EDDM'}, 'dp': {'s': '9', 'b': '511', 't': 'QStatistics', 'd': 'EDDM'}, 'htnb': {}, 'oaue': {'n': '16', 'w': '208'}, 'ozabag': {'s': '10'}, 'rcd': {'s': '1', 'b': '951', 't': '697', 'c': '20', 'd': 'DDM'}}}, 'real': {'elec2': {'ddd': {'w': '1031', 'd': 'DDM'}, 'dp': {'s': '10', 'b': '524', 't': 'QStatistics', 'd': 'DDM'}, 'htnb': {}, 'oaue': {'n': '11', 'w': '204'}, 'ozabag': {'s': '7'}, 'rcd': {'s': '1', 'b': '260', 't': '813', 'c': '20', 'd': 'DDM'}}, 'noaa': {'ddd': {'w': '106', 'd': 'ADWINChangeDetector'}, 'dp': {'s': '10', 'b': '329', 't': 'QStatistics', 'd': 'DDM'}, 'htnb': {}, 'oaue': {'n': '10', 'w': '234'}, 'ozabag': {'s': '6'}, 'rcd': {'s': '1', 'b': '230', 't': '987', 'c': '12', 'd': 'DDM'}}, 'powersupplyDayNight': {'ddd': {'w': '1001', 'd': 'DDM'}, 'dp': {'s': '10', 'b': '222', 't': 'EntropyMeasure', 'd': 'ADWINChangeDetector'}, 'htnb': {}, 'oaue': {'n': '16', 'w': '226'}, 'ozabag': {'s': '16'}, 'rcd': {'s': '1', 'b': '207', 't': '498', 'c': '15', 'd': 'DDM'}}, 'sensorClasses29and31': {'ddd': {'w': '329', 'd': 'DDM'}, 'dp': {'s': '9', 'b': '675', 't': 'QStatistics', 'd': 'EDDM'}, 'htnb': {}, 'oaue': {'n': '10', 'w': '216'}, 'ozabag': {'s': '6'}, 'rcd': {'s': '3', 'b': '480', 't': '536', 'c': '15', 'd': 'DDM'}}, 'weatherAUSprep': {'ddd': {'w': '1073', 'd': 'DDM'}, 'dp': {'s': '10', 'b': '665', 't': 'QStatistics', 'd': 'ADWINChangeDetector'}, 'htnb': {}, 'oaue': {'n': '11', 'w': '235'}, 'ozabag': {'s': '8'}, 'rcd': {'s': '4', 'b': '235', 't': '203', 'c': '11', 'd': 'EDDM'}}}, 'gradual': {'Agrawal': {'ddd': {'w': '1013', 'd': 'DDM'}, 'dp': {'s': '7', 'b': '887', 't': 'EntropyMeasure', 'd': 'EDDM'}, 'htnb': {}, 'oaue': {'n': '14', 'w': '237'}, 'ozabag': {'s': '10'}, 'rcd': {'s': '1', 'b': '243', 't': '684', 'c': '20', 'd': 'EDDM'}}, 'SEA': {'ddd': {'w': '2227', 'd': 'EDDM'}, 'dp': {'s': '10', 'b': '413', 't': 'EntropyMeasure', 'd': 'DDM'}, 'htnb': {}, 'oaue': {'n': '15', 'w': '250'}, 'ozabag': {'s': '13'}, 'rcd': {'s': '3', 'b': '924', 't': '926', 'c': '11', 'd': 'EDDM'}}, 'STAGGER': {'ddd': {'w': '102', 'd': 'EDDM'}, 'dp': {'s': '6', 'b': '356', 't': 'QStatistics', 'd': 'EDDM'}, 'htnb': {}, 'oaue': {'n': '5', 'w': '211'}, 'ozabag': {'s': '10'}, 'rcd': {'s': '3', 'b': '954', 't': '524', 'c': '12', 'd': 'DDM'}}, 'Sine': {'ddd': {'w': '18', 'd': 'DDM'}, 'dp': {'s': '10', 'b': '763', 't': 'QStatistics', 'd': 'DDM'}, 'htnb': {}, 'oaue': {'n': '12', 'w': '219'}, 'ozabag': {'s': '12'}, 'rcd': {'s': '1', 'b': '213', 't': '561', 'c': '14', 'd': 'DDM'}}}}
'''
'''
params = {
'art': {'Agrawal': {'ddd': {'w': '1265', 'd': 'ADWINChangeDetector'}, 'dp': {'s': '10', 'b': '506', 't': 'QStatistics', 'd': 'ADWINChangeDetector'}, 'htnb': {}, 'oaue': {'n': '5', 'w': '220'}, 'ozabag': {'s': '6'}, 'rcd': {'s': '2', 'b': '271', 't': '243', 'c': '12', 'd': 'EDDM'}}, 'SEA': {'ddd': {'w': '1476', 'd': 'EDDM'}, 'dp': {'s': '10', 'b': '204', 't': 'QStatistics', 'd': 'DDM'}, 'htnb': {}, 'oaue': {'n': '14', 'w': '227'}, 'ozabag': {'s': '14'}, 'rcd': {'s': '1', 'b': '201', 't': '498', 'c': '15', 'd': 'ADWINChangeDetector'}}, 'STAGGER': {'ddd': {'w': '118', 'd': 'DDM'}, 'dp': {'s': '10', 'b': '327', 't': 'EntropyMeasure', 'd': 'DDM'}, 'htnb': {}, 'oaue': {'n': '12', 'w': '222'}, 'ozabag': {'s': '6'}, 'rcd': {'s': '2', 'b': '993', 't': '410', 'c': '18', 'd': 'DDM'}}, 'Sine': {'ddd': {'w': '21', 'd': 'EDDM'}, 'dp': {'s': '10', 'b': '518', 't': 'EntropyMeasure', 'd': 'DDM'}, 'htnb': {}, 'oaue': {'n': '16', 'w': '203'}, 'ozabag': {'s': '5'}, 'rcd': {'s': '3', 'b': '955', 't': '368', 'c': '9', 'd': 'DDM'}}}, 
'real': {'elec2': {'ddd': {'w': '1053', 'd': 'DDM'}, 'dp': {'s': '10', 'b': '221', 't': 'QStatistics', 'd': 'DDM'}, 'htnb': {}, 'oaue': {'n': '6', 'w': '246'}, 'ozabag': {'s': '7'}, 'rcd': {'s': '1', 'b': '230', 't': '710', 'c': '15', 'd': 'DDM'}}, 'noaa': {'ddd': {'w': '1035', 'd': 'ADWINChangeDetector'}, 'dp': {'s': '10', 'b': '267', 't': 'QStatistics', 'd': 'DDM'}, 'htnb': {}, 'oaue': {'n': '10', 'w': '254'}, 'ozabag': {'s': '9'}, 'rcd': {'s': '1', 'b': '241', 't': '265', 'c': '20', 'd': 'DDM'}}, 'powersupplyDayNight': {'ddd': {'w': '1021', 'd': 'EDDM'}, 'dp': {'s': '10', 'b': '241', 't': 'EntropyMeasure', 'd': 'DDM'}, 'htnb': {}, 'oaue': {'n': '18', 'w': '230'}, 'ozabag': {'s': '16'}, 'rcd': {'s': '1', 'b': '208', 't': '546', 'c': '20', 'd': 'DDM'}}, 'sensorClasses29and31': {'ddd': {'w': '369', 'd': 'DDM'}, 'dp': {'s': '10', 'b': '320', 't': 'EntropyMeasure', 'd': 'EDDM'}, 'htnb': {}, 'oaue': {'n': '14', 'w': '219'}, 'ozabag': {'s': '6'}, 'rcd': {'s': '3', 'b': '900', 't': '461', 'c': '18', 'd': 'DDM'}}, 'weatherAUSprep': {'ddd': {'w': '2346', 'd': 'DDM'}, 'dp': {'s': '10', 'b': '299', 't': 'EntropyMeasure', 'd': 'DDM'}, 'htnb': {}, 'oaue': {'n': '16', 'w': '229'}, 'ozabag': {'s': '13'}, 'rcd': {'s': '3', 'b': '220', 't': '312', 'c': '12', 'd': 'EDDM'}}}}
'''


# NON UNIFORM AND EXTRA BEST PARAMS WITH NEWEVAL
'''
params = {
'extra': {'digit1': {'ddd': {'w': '287960', 'p': '716493', 'h': '445940', 's': '14', 'd': 'ADWINChangeDetector'}, 'dp': {'s': '10', 'b': '294', 't': 'EntropyMeasure', 'd': 'DDM'}, 'htnb': {}, 'oaue': {'n': '10', 'w': '248'}, 'ozabag': {'s': '15'}, 'rcd': {'s': '39826', 'b': '422', 't': '577', 'c': '19', 'd': 'EDDM'}}, 'g241d': {'ddd': {'w': '114448', 'p': '483021', 'h': '85711', 's': '15', 'd': 'EDDM'}, 'dp': {'s': '10', 'b': '1000', 't': 'EntropyMeasure', 'd': 'EDDM'}, 'htnb': {}, 'oaue': {'n': '10', 'w': '211'}, 'ozabag': {'s': '12'}, 'rcd': {'s': '10020', 'b': '674', 't': '913', 'c': '12', 'd': 'EDDM'}}}, 
'art': {
'Agrawal': {'ddd': {'w': '58996', 'p': '675824', 'h': '490439', 's': '5', 'd': 'DDM'}, 'dp': {'s': '6', 'b': '539', 't': 'QStatistics', 'd': 'EDDM'}, 'htnb': {}, 'oaue': {'n': '9', 'w': '384'}, 'ozabag': {'s': '11'}, 'rcd': {'s': '46140', 'b': '255', 't': '706', 'c': '14', 'd': 'EDDM'}}, 'SEA': {'ddd': {'w': '237163', 'p': '88881', 'h': '196151', 's': '16', 'd': 'DDM'}, 'dp': {'s': '19', 'b': '867', 't': 'QStatistics', 'd': 'ADWINChangeDetector'}, 'htnb': {}, 'oaue': {'n': '9', 'w': '378'}, 'ozabag': {'s': '16'}, 'rcd': {'s': '24342', 'b': '534', 't': '791', 'c': '13', 'd': 'ADWINChangeDetector'}}, 'STAGGER': {'ddd': {'w': '31732', 'p': '428251', 'h': '97615', 's': '20', 'd': 'EDDM'}, 'dp': {'s': '10', 'b': '902', 't': 'EntropyMeasure', 'd': 'DDM'}, 'htnb': {}, 'oaue': {'n': '14', 'w': '251'}, 'ozabag': {'s': '19'}, 'rcd': {'s': '27827', 'b': '827', 't': '252', 'c': '20', 'd': 'ADWINChangeDetector'}}, 'Sine': {'ddd': {'w': '148073', 'p': '788328', 'h': '157662', 's': '20', 'd': 'DDM'}, 'dp': {'s': '15', 'b': '616', 't': 'QStatistics', 'd': 'ADWINChangeDetector'}, 'htnb': {}, 'oaue': {'n': '11', 'w': '232'}, 'ozabag': {'s': '12'}, 'rcd': {'s': '10752', 'b': '987', 't': '794', 'c': '14', 'd': 'ADWINChangeDetector'}}}, 
'real': {
'elec2': {'ddd': {'e': 'lec2', 'w': '20721', 'p': '314002', 'h': '710582', 's': '11', 'd': 'DDM'}, 'dp': {'e': 'lec2', 's': '12', 'b': '391', 't': 'QStatistics', 'd': 'DDM'}, 'htnb': {}, 'oaue': {'e': 'lec2', 'n': '16', 'w': '250'}, 'ozabag': {'e': 'lec2', 's': '8'}, 'rcd': {'e': 'lec2', 's': '10055', 'b': '543', 't': '762', 'c': '18', 'd': 'ADWINChangeDetector'}}, 'noaa': {'ddd': {'n': 'oaa', 'w': '101753', 'p': '834975', 'h': '991338', 's': '18', 'd': 'DDM'}, 'dp': {'n': 'oaa', 's': '10', 'b': '267', 't': 'EntropyMeasure', 'd': 'ADWINChangeDetector'}, 'htnb': {}, 'oaue': {'n': '10', 'w': '201'}, 'ozabag': {'n': 'oaa', 's': '18'}, 'rcd': {'n': 'oaa', 's': '42171', 'b': '219', 't': '286', 'c': '16', 'd': 'ADWINChangeDetector'}}, 'powersupplyDayNight': {'ddd': {'p': '764032', 'w': '100357', 'h': '150614', 's': '12', 'd': 'EDDM'}, 'dp': {'p': 'owersupplyDayNight', 's': '10', 'b': '253', 't': 'EntropyMeasure', 'd': 'DDM'}, 'htnb': {}, 'oaue': {'p': 'owersupplyDayNight', 'n': '10', 'w': '225'}, 'ozabag': {'p': 'owersupplyDayNight', 's': '6'}, 'rcd': {'p': 'owersupplyDayNight', 's': '10001', 'b': '856', 't': '332', 'c': '14', 'd': 'ADWINChangeDetector'}}, 'sensorClasses29and31': {'ddd': {'s': '9', 'w': '251353', 'p': '724588', 'h': '164251', 'd': 'DDM'}, 'dp': {'s': '13', 'b': '312', 't': 'QStatistics', 'd': 'ADWINChangeDetector'}, 'htnb': {}, 'oaue': {'s': 'ensorClasses29and31', 'n': '15', 'w': '271'}, 'ozabag': {'s': '9'}, 'rcd': {'s': '39765', 'b': '350', 't': '887', 'c': '17', 'd': 'DDM'}}}}
'''

# UNI WITH BESTEVAL
params = {'extra': {'digit1': {'ddd': {'w': '463131', 'p': '778169', 'h': '795368', 's': '20', 'd': 'ADWINChangeDetector'}, 'dp': {'s': '10', 'b': '257', 't': 'QStatistics', 'd': 'DDM'}, 'htnb': {}, 'oaue': {'n': '10', 'w': '253'}, 'ozabag': {'s': '20'}, 'rcd': {'s': '10068', 'b': '710', 't': '477', 'c': '20', 'd': 'ADWINChangeDetector'}}, 'g241d': {'ddd': {'w': '22103', 'p': '822556', 'h': '762592', 's': '12', 'd': 'EDDM'}, 'dp': {'s': '10', 'b': '217', 't': 'EntropyMeasure', 'd': 'EDDM'}, 'htnb': {}, 'oaue': {'n': '10', 'w': '201'}, 'ozabag': {'s': '18'}, 'rcd': {'s': '10030', 'b': '897', 't': '688', 'c': '15', 'd': 'ADWINChangeDetector'}}}, 'art': {'Agrawal': {'ddd': {'w': '150860', 'p': '788841', 'h': '547908', 's': '18', 'd': 'EDDM'}, 'dp': {'s': '10', 'b': '206', 't': 'EntropyMeasure', 'd': 'DDM'}, 'htnb': {}, 'oaue': {'n': '18', 'w': '269'}, 'ozabag': {'s': '18'}, 'rcd': {'s': '44365', 'b': '225', 't': '494', 'c': '9', 'd': 'EDDM'}}, 'SEA': {'ddd': {'w': '256589', 'p': '871811', 'h': '323970', 's': '8', 'd': 'DDM'}, 'dp': {'s': '10', 'b': '273', 't': 'EntropyMeasure', 'd': 'DDM'}, 'htnb': {}, 'oaue': {'n': '5', 'w': '200'}, 'ozabag': {'s': '8'}, 'rcd': {'s': '10144', 'b': '422', 't': '809', 'c': '5', 'd': 'ADWINChangeDetector'}}, 'STAGGER': {'ddd': {'w': '3231', 'p': '467478', 'h': '659527', 's': '11', 'd': 'DDM'}, 'dp': {'s': '5', 'b': '345', 't': 'QStatistics', 'd': 'DDM'}, 'htnb': {}, 'oaue': {'n': '5', 'w': '200'}, 'ozabag': {'s': '14'}, 'rcd': {'s': '14371', 'b': '707', 't': '996', 'c': '15', 'd': 'DDM'}}, 'Sine': {'ddd': {'w': '12812', 'p': '990659', 'h': '744652', 's': '17', 'd': 'EDDM'}, 'dp': {'s': '10', 'b': '207', 't': 'QStatistics', 'd': 'DDM'}, 'htnb': {}, 'oaue': {'n': '11', 'w': '200'}, 'ozabag': {'s': '7'}, 'rcd': {'s': '11163', 'b': '686', 't': '575', 'c': '12', 'd': 'DDM'}}}, 'real': {'elec2': {'ddd': {'w': '321436', 'p': '982921', 'h': '47899', 's': '13', 'd': 'DDM'}, 'dp': {'s': '10', 'b': '211', 't': 'EntropyMeasure', 'd': 'DDM'}, 'htnb': {}, 'oaue': {'n': '13', 'w': '211'}, 'ozabag': {'s': '17'}, 'rcd': {'s': '33987', 'b': '537', 't': '206', 'c': '9', 'd': 'DDM'}}, 'noaa': {'ddd': {'w': '45281', 'p': '172733', 'h': '196227', 's': '19', 'd': 'EDDM'}, 'dp': {'s': '10', 'b': '210', 't': 'QStatistics', 'd': 'DDM'}, 'htnb': {}, 'oaue': {'n': '10', 'w': '211'}, 'ozabag': {'s': '9'}, 'rcd': {'s': '42573', 'b': '254', 't': '428', 'c': '14', 'd': 'DDM'}}, 'powersupplyDayNight': {'ddd': {'w': '404005', 'p': '914163', 'h': '467476', 's': '18', 'd': 'EDDM'}, 'dp': {'s': '10', 'b': '223', 't': 'QStatistics', 'd': 'DDM'}, 'htnb': {}, 'oaue': {'n': '5', 'w': '201'}, 'ozabag': {'s': '18'}, 'rcd': {'s': '10094', 'b': '469', 't': '449', 'c': '5', 'd': 'ADWINChangeDetector'}}, 'sensorClasses29and31': {'ddd': {'w': '280545', 'p': '774588', 'h': '12467', 's': '14', 'd': 'DDM'}, 'dp': {'s': '10', 'b': '206', 't': 'EntropyMeasure', 'd': 'EDDM'}, 'htnb': {}, 'oaue': {'n': '15', 'w': '217'}, 'ozabag': {'s': '7'}, 'rcd': {'s': '43516', 'b': '281', 't': '943', 'c': '11', 'd': 'EDDM'}}, 'weatherAUSprep': {'ddd': {'w': '69668', 'p': '588463', 'h': '522778', 's': '11', 'd': 'DDM'}, 'dp': {'s': '10', 'b': '262', 't': 'QStatistics', 'd': 'ADWINChangeDetector'}, 'htnb': {}, 'oaue': {'n': '19', 'w': '202'}, 'ozabag': {'s': '7'}, 'rcd': {'s': '10047', 'b': '949', 't': '851', 'c': '12', 'd': 'DDM'}}}}



streams = {'abrupt':{'Sine':[1,2],'Agrawal':[3,4,5,6],'SEA':[7,8],'STAGGER':[9,10]},
           'gradual':{'Sine':[1,2],'Agrawal':[3,4,5,6],'SEA':[7,8],'STAGGER':[9,10]},
           'real':{'elec2':[1],'noaa':[1],'powersupplyDayNight':[1],'sensorClasses29and31':[1]},
           'extra':{'g241d':[1],'digit1':[1]}
           }
searchart = "../../ArtificialDataSets/{}_matlab/stream{}.arff"
searchartlab = "../../ArtificialDataSets/{}_matlab/lab_{}_stream{}_uni_l{}.csv"
searchreal = "../../realdatasets/realstreams/train_{}.arff"
searchreallab = "../../realdatasets/realstreams/lab_train_{}_uni_l{}.csv"
searchextra = "../../extra/extra_matlab/{}.arff"
searchextralab = "../../extra/extra_matlab/lab_train_{}_l{}.csv"
regularlabels = [5, 10, 20]
extralabels = [10, 100]
methods = ['htnb','ozabag','oaue','rcd','ddd','dp']

for t,s in streams.items():
    if t=='real':
        pt='real'
        labels = regularlabels
    elif t=='extra':
        pt='extra'
        labels = extralabels
    else:
        pt='art'
        labels = regularlabels
    rcsv.write('{}\n'.format(t))
    rtex.write('\\multicolumn{{13}}{{c}}{{Type of drift: {}}} \\\\ \n'.format(t))
    for label in labels:
        rcsv.write('Labels at {}\n'.format(label))
        rtex.write('\\multicolumn{{13}}{{c}}{{Labels at {}}} \\\\ \n'.format(label))
        for data,seqs in s.items():
            for seq in seqs:
                if t=='real':
                    names = sorted(glob(searchreal.format(data)))
                    labnames = sorted(glob(searchreallab.format(data,label)))
                elif t=='extra':
                    names = sorted(glob(searchextra.format(data)))
                    labnames = sorted(glob(searchextralab.format(data,label)))
                else:
                    names = sorted(glob(searchart.format(t,seq)))
                    labnames = sorted(glob(searchartlab.format(t,t,seq,label)))
                for f, lab in zip(names, labnames):
                    onlyname = Path(lab).stem
                    if t == 'real' or t=='extra':
                        rcsv.write(data + ',')
                        rtex.write(data + ' & ')
                    else:
                        rcsv.write(data + str(1+seq-min(seqs)) + ',')
                        rtex.write(data + str(1+seq-min(seqs)) + ' & ')

                    # HTNB
                    if 'htnb' in methods:
                        out = tempdir+'res_htnb_'+onlyname+'.csv'
                        cmd = prefix + ("'EvaluatePrequential -l (trees.HoeffdingTree -l NB) "
                            "-s (ArffFileStream -f {input}) "
                            "-e FadingFactorClassificationPerformanceEvaluator "
                            "-f 1 -d {output} -z {lab}'\n").format(input=f,output=out,lab=lab)
                        writefile(cmd, out, rcsv, rtex, vecdir, False)

                    # OzaBag
                    if 'ozabag' in methods:
                        out = tempdir+'res_ozabag_'+onlyname+'.csv'
                        cmd = prefix + ("'EvaluatePrequential -l (meta.OzaBag -s {s} "
                                "-l (trees.HoeffdingTree -l NB)) "
                                "-s (ArffFileStream -f {input}) "
                                "-e FadingFactorClassificationPerformanceEvaluator "
                                "-f 1 -d {output} -z {lab}'\n").format(input=f,output=out,
                                                lab=lab,s=params[pt][data]['ozabag']['s'])
                        writefile(cmd, out, rcsv, rtex, vecdir, False)

                    # OAUE
                    if 'oaue' in methods:
                        out = tempdir + 'res_oaue_' + onlyname + '.csv'
                        cmd = prefix + ("'EvaluatePrequential -l (meta.OnlineAccuracyUpdatedEnsemble -n {n} "
                                "-w {w} -l (trees.HoeffdingTree -l NB)) -s (ArffFileStream -f {input}) "
                                "-e FadingFactorClassificationPerformanceEvaluator "
                                "-f 1 -d {output} -z {lab}'\n").format(input=f,output=out,lab=lab,
                                                    n=params[pt][data]['oaue']['n'],
                                                    w=params[pt][data]['oaue']['w'])
                        writefile(cmd, out, rcsv, rtex, vecdir, False)

                    # RCD
                    if 'rcd' in methods:
                        out = tempdir+'res_rcd_'+onlyname+'.csv'
                        cmd = prefix + ("'EvaluatePrequential -l (meta.RCD -s {s} -b {b} -t {t} -c {c} "
                                "-l (trees.HoeffdingTree -l NB) -d {d}) -s (ArffFileStream -f {input}) "
                                "-e FadingFactorClassificationPerformanceEvaluator "
                                "-f 1 -d {output} -z {lab}'\n").format(input=f,output=out,lab=lab,
                                            s=float(params[pt][data]['rcd']['s'])/1e6,
                                            b=params[pt][data]['rcd']['b'],
                                            t=params[pt][data]['rcd']['t'],c=params[pt][data]['rcd']['c'],
                                            d=params[pt][data]['rcd']['d'])
                        writefile(cmd, out, rcsv, rtex, vecdir, False)

                    # DDD
                    if 'ddd' in methods:
                        out = tempdir+'res_ddd_'+onlyname+'.csv'
                        cmd = prefix + ("'EvaluatePrequential -l (meta.DiversityForDealingWithDrifts "
                                "-d {d} -w {w} -p {p} -h {h} "
                                "-l (meta.OzaBagDDD -l (trees.HoeffdingTree -l NB) -s {s})) "
                                "-s (ArffFileStream -f {input}) "
                                "-e FadingFactorClassificationPerformanceEvaluator "
                                "-f 1 -d {output} -z {lab}'\n").format(input=f,output=out,lab=lab,
                                            d=params[pt][data]['ddd']['d'],
                                            s=params[pt][data]['ddd']['s'],
                                            w=float(params[pt][data]['ddd']['w'])/1e6,
                                            p=float(params[pt][data]['ddd']['p'])/1e6,
                                            h=float(params[pt][data]['ddd']['h'])/1e6)
                        writefile(cmd, out, rcsv, rtex, vecdir, False)

                    # DP
                    if 'dp' in methods:
                        out = tempdir+'res_dp_'+onlyname+'.csv'
                        cmd = prefix + ("'EvaluatePrequential -l (meta.DiversityPool -s {s} -b {b} -d {d} "
                              "-t {t} -l (trees.HoeffdingTree -l NB)) -s (ArffFileStream -f {input}) "
                              "-e FadingFactorClassificationPerformanceEvaluator "
                              "-f 1 -d {output} -z {lab}'").format(input=f,output=out,lab=lab,
                                            s=params[pt][data]['dp']['s'],
                                            b=params[pt][data]['dp']['b'],
                                            d=params[pt][data]['dp']['d'],
                                            t=params[pt][data]['dp']['t'])
                        writefile(cmd, out, rcsv, rtex, vecdir, True)

rcsv.close()
rtex.close()
