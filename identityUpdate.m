function net = identityUpdate(net, XBatch)
n=size(XBatch,1);
c=size(net.c,1);
if c>n
    net.c(end-n+1:end,:) = XBatch;
else
    net.c = XBatch(end-c+1:end,:);
end
end