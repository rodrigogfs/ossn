function generateStreamLabels(file,typeofdrift,modes,rates,nbursts)
% Generates a binary label mask for semisupervised streams. It has 3 modes: 
% Uniform, FixedBurst and RandomBursts. For FixedBurst
% and RandomBursts, the exact number of labels is not garanteed

[filepath,name,ext] = fileparts(file);
for labelrate = rates
    for m = modes
        mode = m{1};
        X=load(file);
        streamsize=size(X,1);
        portion = ceil(labelrate*streamsize);
        labels = false(streamsize,1);
        %% Nonuniform selection
        if strcmpi(mode,'nonuni') % kmeans selection
            conceptsize=4000;
            start=1; finish=min([start+conceptsize-1, streamsize]);
            k=5;
            sample=[];
            while start<streamsize
               window = start:finish;
               portionperconceptperclass = ceil(labelrate*length(window)/2);
               windowclass0 = window(X(window,end)==0);
               windowclass1 = window(X(window,end)==1);
               if ~isempty(windowclass0)
                   idx0 = kmeans(X(windowclass0,1:end-1),k);              
                   missing = portionperconceptperclass;
                   for c=1:k
                       cluster=windowclass0(idx0==c);
                       if length(cluster)<missing
                           sample=[sample, cluster];
                           missing = missing - length(cluster);
                       else
                           sample=[sample, cluster(1:missing)];
                           break;
                       end
                   end
               end
               if ~isempty(windowclass1)
                   idx1 = kmeans(X(windowclass1,1:end-1),k);
                   missing  = portionperconceptperclass;
                   for c=k:-1:1 % so that we don't take points from similar clusters 
                       cluster=windowclass1(idx1==c);
                       if length(cluster)<missing
                           sample=[sample, cluster];
                           missing = missing - length(cluster);
                       else
                           sample=[sample, cluster(1:missing)];
                           break;
                       end
                   end
               end
               start = start + conceptsize;
               finish = min([start+conceptsize-1, streamsize]);
            end
            labels = false(streamsize,1);
            labels(sample)=true;
            supfile = [filepath filesep 'sup_' typeofdrift name '_' mode '_' 'l' num2str(labelrate*100) '.arff'];
            labfile = [filepath filesep 'lab_' typeofdrift name '_' mode '_' 'l' num2str(labelrate*100) ext];
        %% Uniform selection
        elseif strcmpi(mode,'uni')
            sample = randperm(streamsize,portion);
            labels(sample) = true;
            supfile = [filepath filesep 'sup_' typeofdrift name '_' mode '_' 'l' num2str(labelrate*100) '.arff'];
            labfile = [filepath filesep 'lab_' typeofdrift name '_' mode '_' 'l' num2str(labelrate*100) ext];
        elseif strcmpi(mode,'fb')
            step = round(streamsize / (nbursts+1));
            location = zeros(1,nbursts);
            burstsize=round(step/4);
            for j=1:nbursts
                location(j)=j*step;
            end
            for j=1:portion
                loc = randi(nbursts);
                sample = round(randn*burstsize+location(loc));
                if sample>0 && sample<=streamsize
                    labels(sample) = true;
                end
            end
            supfile = [filepath filesep 'sup_' typeofdrift name '_' mode '_' 'nb' num2str(nbursts) '_' 'l' num2str(labelrate*100) '.arff'];
            labfile = [filepath filesep 'lab_' typeofdrift name '_' mode '_' 'nb' num2str(nbursts) '_' 'l' num2str(labelrate*100) ext];
        %% Random gaussian bursts
        elseif strcmpi(mode,'rb') %random bursts 
            step = round(streamsize / (nbursts+1));
            burstsize=round(step/4);
            location = randi(streamsize,1,nbursts);
            for j=1:portion
                loc = randi(nbursts);
                sample = round(randn*burstsize+location(loc));
                if sample>0 && sample<=streamsize
                    labels(sample) = true;
                end
            end
            supfile = [filepath filesep 'sup_' typeofdrift name '_' mode '_' 'nb' num2str(nbursts) '_' 'l' num2str(labelrate*100) '.arff'];
            labfile = [filepath filesep 'lab_' typeofdrift name '_' mode '_' 'nb' num2str(nbursts) '_' 'l' num2str(labelrate*100) ext]; 
        else
            disp('Invalid label generation mode');
        end
        %% arff header
        disp(labelrate);
        disp(sum(labels)/streamsize);
        header=['@RELATION ' name '\n\n'];
        for i=1:size(X,2)-1
            header = strcat(header, '@ATTRIBUTE a', num2str(i), ' numeric\n');
        end
        header = strcat(header, '@ATTRIBUTE class {0,1}\n\n');
        header = strcat(header, '@DATA\n');
        fid = fopen(supfile,'w');
        fprintf(fid, header);
        fclose(fid);
        %% Writing files
        dlmwrite(supfile,X(labels,:),'-append');
        dlmwrite(labfile,labels);
    end
end
end