function [net, losses, eta] = trainOnlineSSL_NR(net, fp, Xlab, lY, Xunl)
% This is the supervised part of the training algorithm (Newton)
STOP_CRITERION	= 1e-6; % minimum gradient norm
ETA_MIN	= 2^(-8); % minimum learning rate
Xunl = [net.c; Xunl]; % centres are considered unlabeled instances
[lF, lPhi]=rbfforward(net,Xlab);
[uF, uPhi]=rbfforward(net,Xunl);
% adding biases
lPhi=[lPhi ones(size(Xlab,1),1)]; % N x M
uPhi=[uPhi ones(size(Xunl,1),1)];
lPhi_t=lPhi'; % M x N
uPhi_t=uPhi';
w = [net.w; net.bias];
numL = size(Xlab,1); numU=size(Xunl,1);
% L2 regularization scaling factor
if numL
    m = 1/numL + 1/numU;
else
    m = 1/numU;
end
numW = size(w,1); % number of weights of one neuron
% regularization matrix for hessian
A=diag((fp.alpha*m)*ones(numel(w),1));
losses = zeros(fp.epochs,1);
% Calculating similarities for pseudolabels
% The labelled instances are considered for the neighborhood
dist2 = squaredDistTwoSets(Xunl,[Xlab; Xunl]);
dist2(dist2==0)=Inf;
sigma = fp.sigmamanif*sqrt(min(dist2,[],2)) + 1e-5;
S = exp(- dist2 ./ (2*sigma.^2) );

%% initial loss
u = ( (S*[lY; uF]) ./ (sum(S,2)+1e-5) ); % with safeguard
lossU = - fp.lambda * sum( u.*log(uF) + (1-u).*log(1-uF) ) / numU;
if numL
    lossL = - sum( lY.*log(lF) + (1-lY).*log(1-lF) ) / numL;
else
    lossL = 0;
end
lossW = 0.5 * fp.alpha * m * sum(w.^2);
loss_new = lossL + lossU + lossW;

%% iterations of irls
for i=1:fp.epochs
    %% gradient and hessian
    % unlabelled terms
    uG = fp.lambda * (uPhi_t* ( ( uF - u)) ) / numU;
    uH = fp.lambda * (sum(uF.*(1-uF)) * uPhi_t * uPhi) / numU;
    % labelled terms depend on the presence of labels
    if numL
        lG = (lPhi_t*(lF-lY)) / numL;
        lH = (sum(lF.*(1-lF)) * lPhi_t * lPhi) / numL;
        G = lG + uG + fp.alpha*m*w;
        H = lH + uH + A;
    else
        G = uG + fp.alpha*m*w;
        H = uH + A;
    end
    %% Updating loss log
    losses(i)	= loss_new;
    %% Stopping criteria
    if i>1 && norm(G)/numW<STOP_CRITERION
        losses	= losses(1:i);
        break;
    end
    %% Hessian
    Hinv = chol(H);     %  chol descomposition
    delta_w	= Hinv \ (Hinv' \ G);  %  incremental weights
    eta	= 1; % learning rate
    
    %% optimising best fit of RBF using eta (Newton-Raphson line search)
    while eta>ETA_MIN 
        w_new	= w - eta*delta_w;
        lF=sigmoid(lPhi*w_new);
        uF=sigmoid(uPhi*w_new);
        if any(any(~isfinite(lF))) || any(any(~isfinite(uF)))
            eta	= eta/2; % Armijo condition update
            continue;
        end
        % current loss
        lossU = - fp.lambda * sum( u.*log(uF) + (1-u).*log(1-uF) ) / numU;
        if numL
            lossL = - sum( lY.*log(lF) + (1-lY).*log(1-lF) ) / numL;
        else
            lossL = 0;
        end
        lossW = 0.5 * fp.alpha * m * sum(w_new.^2);
        loss_new = lossL + lossU + lossW;

        if loss_new>losses(i) || ~isfinite(loss_new)
          eta	= eta/2;
        else
          w		= w_new;
          eta	= 0;
          u = ( (S*[lY; uF]) ./ (sum(S,2)+1e-5) ); % with safeguard
        end
    end
    
    if eta
        break;
    end
    
end
net.w=w(1:net.nhidden);
net.bias=w(end);
end
% 
% function U = putativeLabels(Xunl,Xlab,lY)
% dist2 = squaredDistTwoSets(Xunl,Xlab);
% sigma = 2*min(dist2,[],2)+1e-5;
% S = exp(- dist2 ./ sigma );
% U = (S*lY) ./ (sum(S,2)+1e-5); % with safeguard
% end

% function U = pseudoLabels(X,f,fp)
% dist2 = squaredDistTwoSets(X,X);
% sigma = fp.sigmarate*mean(mean(dist2));
% S = exp(- dist2 ./ sigma );
% U = (S*f) ./ (sum(S,2)+1e-8); % with safeguard
% end