function generateGraphics()

%% ABRUPT %%
%% 5 %
%clear all
linewidth = 1;
markersize = 5;
titfontsize=22;
legfontsize=18;
axisfontsize=20;
smooth_window = 200;
smooth_method = 'movmean';
streched = true;
str_name = '';
path = 'C:\Users\Rodrigo\Documents\programming\onlinessl\figs\newfigs\normal\';
if streched
    linewidth = 2;
    str_name = '_stretched';
    path = 'C:\Users\Rodrigo\Documents\programming\onlinessl\figs\newfigs\stretched\';
end
thebest = 'DDD';
theworst = 'OAUE';
osnn = load('C:\Users\Rodrigo\Documents\programming\onlinessl\test_set4_neweval_nonuni_manifold\Sine_abrupt_5_2_manifold.csv');
worst = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_nonuni_neweval\\res_%s_lab_abrupt_stream2_nonuni_l5_vec.csv',lower(theworst)));
best = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_nonuni_neweval\\res_%s_lab_abrupt_stream2_nonuni_l5_vec.csv',lower(thebest)));
htnb = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_nonuni_neweval\\res_%s_lab_abrupt_stream2_nonuni_l5_vec.csv','htnb'));
osnn = smoothdata(osnn,smooth_method,smooth_window);
worst = smoothdata(worst,smooth_method,smooth_window);
best = smoothdata(best,smooth_method,smooth_window);
htnb = smoothdata(htnb,smooth_method,smooth_window);
s = length(best);
step = 4000;
figure; hold on; box on; set(gca,'fontsize',18);
title('Sine2 - Abrupt - 5% of labels', 'FontSize', titfontsize);
xlabel('Time step', 'FontSize', axisfontsize);
ylabel('Prequential accuracy (%)', 'FontSize', axisfontsize);
plot((100:length(htnb)),htnb(100:end),'-.og','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((100:length(worst)),worst(100:end),':*r','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((100:length(best)),best(100:end),'--+b','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((100:length(osnn)),osnn(100:end),'-sk','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
i=step;
while i<s
    line([i i], get(gca, 'ylim'),'Color','black','LineStyle','--');
    i = i + step;
end
legend({'HTNB',theworst,thebest,'OSNN'}, 'Location', 'southwest', 'FontSize', legfontsize);
if streched
    set(gcf,'position',[1,1,1366,433]);
    set(gca,'position',[0.06588579795022,0.209459453901729,0.917276720351391,0.684189197404964]);
end
saveas(gcf,[path 'nonuni_abrupt_sine2_l5' str_name '.eps'],'epsc');
saveas(gcf,[path 'nonuni_abrupt_sine2_l5' str_name '.fig']);
saveas(gcf,[path 'nonuni_abrupt_sine2_l5' str_name '.png']);
hold off

%% 10 %
%clear all
thebest = 'DDD';
theworst = 'OAUE';
osnn = load('C:\Users\Rodrigo\Documents\programming\onlinessl\test_set4_neweval_nonuni_manifold\Sine_abrupt_10_2_manifold.csv');
worst = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_nonuni_neweval\\res_%s_lab_abrupt_stream2_nonuni_l10_vec.csv',lower(theworst)));
best = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_nonuni_neweval\\res_%s_lab_abrupt_stream2_nonuni_l10_vec.csv',lower(thebest)));
htnb = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_nonuni_neweval\\res_%s_lab_abrupt_stream2_nonuni_l10_vec.csv','htnb'));
osnn = smoothdata(osnn,smooth_method,smooth_window);
worst = smoothdata(worst,smooth_method,smooth_window);
best = smoothdata(best,smooth_method,smooth_window);
htnb = smoothdata(htnb,smooth_method,smooth_window);
s = length(best);
step = 4000;
figure; hold on; box on; set(gca,'fontsize',18);
title('Sine2 - Abrupt - 10% of labels', 'FontSize', titfontsize);
xlabel('Time step', 'FontSize', axisfontsize);
ylabel('Prequential accuracy (%)', 'FontSize', axisfontsize);
plot((100:length(htnb)),htnb(100:end),'-.og','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((100:length(worst)),worst(100:end),':*r','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((100:length(best)),best(100:end),'--+b','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((100:length(osnn)),osnn(100:end),'-sk','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
i=step;
while i<s
    line([i i], get(gca, 'ylim'),'Color','black','LineStyle','--');
    i = i + step;
end
legend({'HTNB',theworst,thebest,'OSNN'}, 'Location', 'southwest', 'FontSize', legfontsize);
if streched
    set(gcf,'position',[1,1,1366,433]);
    set(gca,'position',[0.06588579795022,0.209459453901729,0.917276720351391,0.684189197404964]);
end
saveas(gcf,[path 'nonuni_abrupt_sine2_l10' str_name '.eps'],'epsc');
saveas(gcf,[path 'nonuni_abrupt_sine2_l10' str_name '.fig']);
saveas(gcf,[path 'nonuni_abrupt_sine2_l10' str_name '.png']);
hold off

%% 20 %
%clear all
thebest = 'DDD';
theworst = 'OAUE';
osnn = load('C:\Users\Rodrigo\Documents\programming\onlinessl\test_set4_neweval_nonuni_manifold\Sine_abrupt_20_2_manifold.csv');
worst = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_nonuni_neweval\\res_%s_lab_abrupt_stream2_nonuni_l20_vec.csv',lower(theworst)));
best = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_nonuni_neweval\\res_%s_lab_abrupt_stream2_nonuni_l20_vec.csv',lower(thebest)));
htnb = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_nonuni_neweval\\res_%s_lab_abrupt_stream2_nonuni_l20_vec.csv','htnb'));
osnn = smoothdata(osnn,smooth_method,smooth_window);
worst = smoothdata(worst,smooth_method,smooth_window);
best = smoothdata(best,smooth_method,smooth_window);
htnb = smoothdata(htnb,smooth_method,smooth_window);
s = length(best);
step = 4000;
figure; hold on; box on; set(gca,'fontsize',18);
title('Sine2 - Abrupt - 20% of labels', 'FontSize', titfontsize);
xlabel('Time step', 'FontSize', axisfontsize);
ylabel('Prequential accuracy (%)', 'FontSize', axisfontsize);
plot((100:length(htnb)),htnb(100:end),'-.og','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((100:length(worst)),worst(100:end),':*r','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((100:length(best)),best(100:end),'--+b','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((100:length(osnn)),osnn(100:end),'-sk','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
i=step;
while i<s
    line([i i], get(gca, 'ylim'),'Color','black','LineStyle','--');
    i = i + step;
end
legend({'HTNB',theworst,thebest,'OSNN'}, 'Location', 'southwest', 'FontSize', legfontsize);
if streched
    set(gcf,'position',[1,1,1366,433]);
    set(gca,'position',[0.06588579795022,0.209459453901729,0.917276720351391,0.684189197404964]);
end
saveas(gcf,[path 'nonuni_abrupt_sine2_l20' str_name '.eps'],'epsc');
saveas(gcf,[path 'nonuni_abrupt_sine2_l20' str_name '.fig']);
saveas(gcf,[path 'nonuni_abrupt_sine2_l20' str_name '.png']);
hold off

%% GRADUAL
%% 5 %
%clear all
thebest = 'DDD';
theworst = 'OAUE';
osnn = load('C:\Users\Rodrigo\Documents\programming\onlinessl\test_set4_neweval_nonuni_manifold\Sine_gradual_5_2_manifold.csv');
worst = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_nonuni_neweval\\res_%s_lab_gradual_stream2_nonuni_l5_vec.csv',lower(theworst)));
best = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_nonuni_neweval\\res_%s_lab_gradual_stream2_nonuni_l5_vec.csv',lower(thebest)));
htnb = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_nonuni_neweval\\res_%s_lab_gradual_stream2_nonuni_l5_vec.csv','htnb'));
osnn = smoothdata(osnn,smooth_method,smooth_window);
worst = smoothdata(worst,smooth_method,smooth_window);
best = smoothdata(best,smooth_method,smooth_window);
htnb = smoothdata(htnb,smooth_method,smooth_window);
s = length(best);
step = 4000;
figure; hold on; box on; set(gca,'fontsize',18);
title('Sine2 - Gradual - 5% of labels', 'FontSize', titfontsize);
xlabel('Time step', 'FontSize', axisfontsize);
ylabel('Prequential accuracy (%)', 'FontSize', axisfontsize);
plot((100:length(htnb)),htnb(100:end),'-.og','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((100:length(worst)),worst(100:end),':*r','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((100:length(best)),best(100:end),'--+b','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((100:length(osnn)),osnn(100:end),'-sk','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
i=step;
while i<s
    line([i i], get(gca, 'ylim'),'Color','black','LineStyle','--');
    i = i + step;
end
legend({'HTNB',theworst,thebest,'OSNN'},'Location','southwest', 'FontSize', legfontsize);
if streched
    set(gcf,'position',[1,1,1366,433]);
    set(gca,'position',[0.06588579795022,0.209459453901729,0.917276720351391,0.684189197404964]);
end
saveas(gcf,[path 'nonuni_gradual_sine2_l5' str_name '.eps'],'epsc');
saveas(gcf,[path 'nonuni_gradual_sine2_l5' str_name '.fig']);
saveas(gcf,[path 'nonuni_gradual_sine2_l5' str_name '.png']);
hold off

%% 10 %
%clear all
thebest = 'DDD';
theworst = 'OAUE';
osnn = load('C:\Users\Rodrigo\Documents\programming\onlinessl\test_set4_neweval_nonuni_manifold\Sine_gradual_10_2_manifold.csv');
worst = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_nonuni_neweval\\res_%s_lab_gradual_stream2_nonuni_l10_vec.csv',lower(theworst)));
best = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_nonuni_neweval\\res_%s_lab_gradual_stream2_nonuni_l10_vec.csv',lower(thebest)));
htnb = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_nonuni_neweval\\res_%s_lab_gradual_stream2_nonuni_l10_vec.csv','htnb'));
osnn = smoothdata(osnn,smooth_method,smooth_window);
worst = smoothdata(worst,smooth_method,smooth_window);
best = smoothdata(best,smooth_method,smooth_window);
htnb = smoothdata(htnb,smooth_method,smooth_window);
s = length(best);
step = 4000;
figure; hold on; box on; set(gca,'fontsize',18);
title('Sine2 - Gradual - 10% of labels', 'FontSize', titfontsize);
xlabel('Time step', 'FontSize', axisfontsize);
ylabel('Prequential accuracy (%)', 'FontSize', axisfontsize);
plot((100:length(htnb)),htnb(100:end),'-.og','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((100:length(worst)),worst(100:end),':*r','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((100:length(best)),best(100:end),'--+b','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((100:length(osnn)),osnn(100:end),'-sk','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
i=step;
while i<s
    line([i i], get(gca, 'ylim'),'Color','black','LineStyle','--');
    i = i + step;
end
legend({'HTNB',theworst,thebest,'OSNN'},'Location','southwest', 'FontSize', legfontsize);
if streched
    set(gcf,'position',[1,1,1366,433]);
    set(gca,'position',[0.06588579795022,0.209459453901729,0.917276720351391,0.684189197404964]);
end
saveas(gcf,[path 'nonuni_gradual_sine2_l10' str_name '.eps'],'epsc');
saveas(gcf,[path 'nonuni_gradual_sine2_l10' str_name '.fig']);
saveas(gcf,[path 'nonuni_gradual_sine2_l10' str_name '.png']);
hold off

%% 20 %
%clear all
thebest = 'DDD';
theworst = 'OAUE';
osnn = load('C:\Users\Rodrigo\Documents\programming\onlinessl\test_set4_neweval_nonuni_manifold\Sine_gradual_20_2_manifold.csv');
worst = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_nonuni_neweval\\res_%s_lab_gradual_stream2_nonuni_l20_vec.csv',lower(theworst)));
best = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_nonuni_neweval\\res_%s_lab_gradual_stream2_nonuni_l20_vec.csv',lower(thebest)));
htnb = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_nonuni_neweval\\res_%s_lab_gradual_stream2_nonuni_l20_vec.csv','htnb'));
osnn = smoothdata(osnn,smooth_method,smooth_window);
worst = smoothdata(worst,smooth_method,smooth_window);
best = smoothdata(best,smooth_method,smooth_window);
htnb = smoothdata(htnb,smooth_method,smooth_window);
s = length(best);
step = 4000;
figure; hold on; box on; set(gca,'fontsize',18);
title('Sine2 - Gradual - 20% of labels', 'FontSize', titfontsize);
xlabel('Time step', 'FontSize', axisfontsize);
ylabel('Prequential accuracy (%)', 'FontSize', axisfontsize);
plot((100:length(htnb)),htnb(100:end),'-.og','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((100:length(worst)),worst(100:end),':*r','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((100:length(best)),best(100:end),'--+b','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((100:length(osnn)),osnn(100:end),'-sk','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
i=step;
while i<s
    line([i i], get(gca, 'ylim'),'Color','black','LineStyle','--');
    i = i + step;
end
legend({'HTNB',theworst,thebest,'OSNN'},'Location','southwest', 'FontSize', legfontsize);
if streched
    set(gcf,'position',[1,1,1366,433]);
    set(gca,'position',[0.06588579795022,0.209459453901729,0.917276720351391,0.684189197404964]);
end
saveas(gcf,[path 'nonuni_gradual_sine2_l20' str_name '.eps'],'epsc');
saveas(gcf,[path 'nonuni_gradual_sine2_l20' str_name '.fig']);
saveas(gcf,[path 'nonuni_gradual_sine2_l20' str_name '.png']);
hold off

%% REAL
%% 5 %
%clear all
thebest = 'DP';
theworst = 'RCD';
osnn = load('C:\Users\Rodrigo\Documents\programming\onlinessl\test_set4_neweval_nonuni_manifold\noaa_real_5_1_manifold.csv');
worst = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_nonuni_neweval\\res_%s_lab_train_noaa_nonuni_l5_vec.csv',lower(theworst)));
best = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_nonuni_neweval\\res_%s_lab_train_noaa_nonuni_l5_vec.csv',lower(thebest)));
htnb = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_nonuni_neweval\\res_%s_lab_train_noaa_nonuni_l5_vec.csv','htnb'));
osnn = smoothdata(osnn,smooth_method,smooth_window);
worst = smoothdata(worst,smooth_method,smooth_window);
best = smoothdata(best,smooth_method,smooth_window);
htnb = smoothdata(htnb,smooth_method,smooth_window);
s = length(best);
figure; hold on; box on; set(gca,'fontsize',18);
title('NOAA - 5% of labels', 'FontSize', titfontsize);
xlabel('Time step', 'FontSize', axisfontsize);
ylabel('Prequential accuracy (%)', 'FontSize', axisfontsize);
plot((200:length(htnb)),htnb(200:end),'-.og','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((200:length(worst)),worst(200:end),':*r','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((200:length(best)),best(200:end),'--+b','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((200:length(osnn)),osnn(200:end),'-sk','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
legend({'HTNB',theworst,thebest,'OSNN'}, 'Location','southwest', 'FontSize', legfontsize);
if streched
    set(gcf,'position',[1,1,1366,433]);
    set(gca,'position',[0.06588579795022,0.209459453901729,0.917276720351391,0.684189197404964]);
end
ax = gca;
ax.XAxis.Exponent = 4;
saveas(gcf,[path 'nonuni_real_noaa_l5' str_name '.eps'],'epsc');
saveas(gcf,[path 'nonuni_real_noaa_l5' str_name '.fig']);
saveas(gcf,[path 'nonuni_real_noaa_l5' str_name '.png']);
hold off

%% 10%
%clear all
thebest = 'DP';
theworst = 'RCD';
osnn = load('C:\Users\Rodrigo\Documents\programming\onlinessl\test_set4_neweval_nonuni_manifold\noaa_real_10_1_manifold.csv');
worst = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_nonuni_neweval\\res_%s_lab_train_noaa_nonuni_l10_vec.csv',lower(theworst)));
best = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_nonuni_neweval\\res_%s_lab_train_noaa_nonuni_l10_vec.csv',lower(thebest)));
htnb = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_nonuni_neweval\\res_%s_lab_train_noaa_nonuni_l10_vec.csv','htnb'));
osnn = smoothdata(osnn,smooth_method,smooth_window);
worst = smoothdata(worst,smooth_method,smooth_window);
best = smoothdata(best,smooth_method,smooth_window);
htnb = smoothdata(htnb,smooth_method,smooth_window);
s = length(best);
figure; hold on; box on; set(gca,'fontsize',18);
title('NOAA - 10% of labels', 'FontSize', titfontsize);
xlabel('Time step', 'FontSize', axisfontsize);
ylabel('Prequential accuracy (%)', 'FontSize', axisfontsize);
plot((200:length(htnb)),htnb(200:end),'-.og','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((200:length(worst)),worst(200:end),':*r','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((200:length(best)),best(200:end),'--+b','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((200:length(osnn)),osnn(200:end),'-sk','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
legend({'HTNB',theworst,thebest,'OSNN'}, 'Location','southwest', 'FontSize', legfontsize);
if streched
    set(gcf,'position',[1,1,1366,433]);
    set(gca,'position',[0.06588579795022,0.209459453901729,0.917276720351391,0.684189197404964]);
end
ax = gca;
ax.XAxis.Exponent = 4;
saveas(gcf,[path 'nonuni_real_noaa_l10' str_name '.eps'],'epsc');
saveas(gcf,[path 'nonuni_real_noaa_l10' str_name '.fig']);
saveas(gcf,[path 'nonuni_real_noaa_l10' str_name '.png']);
hold off

%% 20 %
%clear all
thebest = 'DP';
theworst = 'RCD';
osnn = load('C:\Users\Rodrigo\Documents\programming\onlinessl\test_set4_neweval_nonuni_manifold\noaa_real_20_1_manifold.csv');
worst = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_nonuni_neweval\\res_%s_lab_train_noaa_nonuni_l20_vec.csv',lower(theworst)));
best = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_nonuni_neweval\\res_%s_lab_train_noaa_nonuni_l20_vec.csv',lower(thebest)));
htnb = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_nonuni_neweval\\res_%s_lab_train_noaa_nonuni_l20_vec.csv','htnb'));
osnn = smoothdata(osnn,smooth_method,smooth_window);
worst = smoothdata(worst,smooth_method,smooth_window);
best = smoothdata(best,smooth_method,smooth_window);
htnb = smoothdata(htnb,smooth_method,smooth_window);
s = length(best);
figure; hold on; box on; set(gca,'fontsize',18);
title('NOAA - 20% of labels', 'FontSize', titfontsize);
xlabel('Time step', 'FontSize', axisfontsize);
ylabel('Prequential accuracy (%)', 'FontSize', axisfontsize);
plot((200:length(htnb)),htnb(200:end),'-.og','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((200:length(worst)),worst(200:end),':*r','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((200:length(best)),best(200:end),'--+b','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((200:length(osnn)),osnn(200:end),'-sk','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
legend({'HTNB',theworst,thebest,'OSNN'}, 'Location','southwest', 'FontSize', legfontsize);
if streched
    set(gcf,'position',[1,1,1366,433]);
    set(gca,'position',[0.06588579795022,0.209459453901729,0.917276720351391,0.684189197404964]);
end
ax = gca;
ax.XAxis.Exponent = 4;
saveas(gcf,[path 'nonuni_real_noaa_l20' str_name '.eps'],'epsc');
saveas(gcf,[path 'nonuni_real_noaa_l20' str_name '.fig']);
saveas(gcf,[path 'nonuni_real_noaa_l20' str_name '.png']);
hold off


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% UNIFORM
%% ABRUPT %%
%% 5 %
%clear all
thebest = 'DP';
theworst = 'OAUE';
osnn = load('C:\Users\Rodrigo\Documents\programming\onlinessl\test_set3_neweval_uni_manifold\Agrawal_abrupt_5_3_manifold.csv');
worst = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_uni_neweval\\res_%s_lab_abrupt_stream3_uni_l5_vec.csv',lower(theworst)));
best = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_uni_neweval\\res_%s_lab_abrupt_stream3_uni_l5_vec.csv',lower(thebest)));
htnb = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_uni_neweval\\res_%s_lab_abrupt_stream3_uni_l5_vec.csv','htnb'));
osnn = smoothdata(osnn,smooth_method,smooth_window);
worst = smoothdata(worst,smooth_method,smooth_window);
best = smoothdata(best,smooth_method,smooth_window);
htnb = smoothdata(htnb,smooth_method,smooth_window);
s = length(best);
step = 4000;
figure; hold on; box on; set(gca,'fontsize',18);
title('Agrawal1-Abrupt-5% of labels', 'FontSize', titfontsize);
xlabel('Time step', 'FontSize', axisfontsize);
ylabel('Prequential accuracy (%)', 'FontSize', axisfontsize);
plot((100:length(htnb)),htnb(100:end),'-.og','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((100:length(worst)),worst(100:end),':*r','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((100:length(best)),best(100:end),'--+b','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((100:length(osnn)),osnn(100:end),'-sk','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
i=step;
while i<s
    line([i i], get(gca, 'ylim'),'Color','black','LineStyle','--');
    i = i + step;
end
legend({'HTNB',theworst,thebest,'OSNN'},'Location','northwest', 'FontSize', legfontsize);
if streched
    set(gcf,'position',[1,1,1366,433]);
    set(gca,'position',[0.06588579795022,0.209459453901729,0.917276720351391,0.684189197404964]);
end
saveas(gcf,[path 'uni_abrupt_agrawal1_l5' str_name '.eps'],'epsc');
saveas(gcf,[path 'uni_abrupt_agrawal1_l5' str_name '.fig']);
saveas(gcf,[path 'uni_abrupt_agrawal1_l5' str_name '.png']);
hold off

%% 10 %
%clear all
thebest = 'DP';
theworst = 'OAUE';
osnn = load('C:\Users\Rodrigo\Documents\programming\onlinessl\test_set3_neweval_uni_manifold\Agrawal_abrupt_10_3_manifold.csv');
worst = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_uni_neweval\\res_%s_lab_abrupt_stream3_uni_l10_vec.csv',lower(theworst)));
best = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_uni_neweval\\res_%s_lab_abrupt_stream3_uni_l10_vec.csv',lower(thebest)));
htnb = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_uni_neweval\\res_%s_lab_abrupt_stream3_uni_l10_vec.csv','htnb'));
osnn = smoothdata(osnn,smooth_method,smooth_window);
worst = smoothdata(worst,smooth_method,smooth_window);
best = smoothdata(best,smooth_method,smooth_window);
htnb = smoothdata(htnb,smooth_method,smooth_window);
s = length(best);
step = 4000;
figure; hold on; box on; set(gca,'fontsize',18);
title('Agrawal1-Abrupt-10% of labels', 'FontSize', titfontsize);
xlabel('Time step', 'FontSize', axisfontsize);
ylabel('Prequential accuracy (%)', 'FontSize', axisfontsize);
plot((100:length(htnb)),htnb(100:end),'-.og','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((100:length(worst)),worst(100:end),':*r','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((100:length(best)),best(100:end),'--+b','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((100:length(osnn)),osnn(100:end),'-sk','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
i=step;
while i<s
    line([i i], get(gca, 'ylim'),'Color','black','LineStyle','--');
    i = i + step;
end
legend({'HTNB',theworst,thebest,'OSNN'},'Location','northwest', 'FontSize', legfontsize);
if streched
    set(gcf,'position',[1,1,1366,433]);
    set(gca,'position',[0.06588579795022,0.209459453901729,0.917276720351391,0.684189197404964]);
end
saveas(gcf,[path 'uni_abrupt_agrawal1_l10' str_name '.eps'],'epsc');
saveas(gcf,[path 'uni_abrupt_agrawal1_l10' str_name '.fig']);
saveas(gcf,[path 'uni_abrupt_agrawal1_l10' str_name '.png']);
hold off

%% 20 %
%clear all
thebest = 'DP';
theworst = 'OAUE';
osnn = load('C:\Users\Rodrigo\Documents\programming\onlinessl\test_set3_neweval_uni_manifold\Agrawal_abrupt_5_3_manifold.csv');
worst = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_uni_neweval\\res_%s_lab_abrupt_stream3_uni_l20_vec.csv',lower(theworst)));
best = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_uni_neweval\\res_%s_lab_abrupt_stream3_uni_l20_vec.csv',lower(thebest)));
htnb = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_uni_neweval\\res_%s_lab_abrupt_stream3_uni_l20_vec.csv','htnb'));
osnn = smoothdata(osnn,smooth_method,smooth_window);
worst = smoothdata(worst,smooth_method,smooth_window);
best = smoothdata(best,smooth_method,smooth_window);
htnb = smoothdata(htnb,smooth_method,smooth_window);
s = length(best);
step = 4000;
figure; hold on; box on; set(gca,'fontsize',18);
title('Agrawal1-Abrupt-20% of labels', 'FontSize', titfontsize);
xlabel('Time step', 'FontSize', axisfontsize);
ylabel('Prequential accuracy (%)', 'FontSize', axisfontsize);
plot((100:length(htnb)),htnb(100:end),'-.og','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((100:length(worst)),worst(100:end),':*r','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((100:length(best)),best(100:end),'--+b','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((100:length(osnn)),osnn(100:end),'-sk','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
i=step;
while i<s
    line([i i], get(gca, 'ylim'),'Color','black','LineStyle','--');
    i = i + step;
end
legend({'HTNB',theworst,thebest,'OSNN'},'Location','northwest', 'FontSize', legfontsize);
if streched
    set(gcf,'position',[1,1,1366,433]);
    set(gca,'position',[0.06588579795022,0.209459453901729,0.917276720351391,0.684189197404964]);
end
saveas(gcf,[path 'uni_abrupt_agrawal1_l20' str_name '.eps'],'epsc');
saveas(gcf,[path 'uni_abrupt_agrawal1_l20' str_name '.fig']);
saveas(gcf,[path 'uni_abrupt_agrawal1_l20' str_name '.png']);
hold off

%% GRADUAL
%% 5 %
%clear all
thebest = 'DP';
theworst = 'OAUE';
osnn = load('C:\Users\Rodrigo\Documents\programming\onlinessl\test_set3_neweval_uni_manifold\Agrawal_gradual_5_3_manifold.csv');
worst = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_uni_neweval\\res_%s_lab_gradual_stream3_uni_l5_vec.csv',lower(theworst)));
best = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_uni_neweval\\res_%s_lab_gradual_stream3_uni_l5_vec.csv',lower(thebest)));
htnb = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_uni_neweval\\res_%s_lab_gradual_stream3_uni_l5_vec.csv','htnb'));
osnn = smoothdata(osnn,smooth_method,smooth_window);
worst = smoothdata(worst,smooth_method,smooth_window);
best = smoothdata(best,smooth_method,smooth_window);
htnb = smoothdata(htnb,smooth_method,smooth_window);
s = length(best);
step = 4000;
figure; hold on; box on; set(gca,'fontsize',18);
title('Agrawal1-Gradual-5% of labels', 'FontSize', titfontsize);
xlabel('Time step', 'FontSize', axisfontsize);
ylabel('Prequential accuracy (%)', 'FontSize', axisfontsize);
plot((100:length(htnb)),htnb(100:end),'-.og','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((100:length(worst)),worst(100:end),':*r','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((100:length(best)),best(100:end),'--+b','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((100:length(osnn)),osnn(100:end),'-sk','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
i=step;
while i<s
    line([i i], get(gca, 'ylim'),'Color','black','LineStyle','--');
    i = i + step;
end
legend({'HTNB',theworst,thebest,'OSNN'},'Location','northwest', 'FontSize', legfontsize);
if streched
    set(gcf,'position',[1,1,1366,433]);
    set(gca,'position',[0.06588579795022,0.209459453901729,0.917276720351391,0.684189197404964]);
end
saveas(gcf,[path 'uni_gradual_agrawal1_l5' str_name '.eps'],'epsc');
saveas(gcf,[path 'uni_gradual_agrawal1_l5' str_name '.fig']);
saveas(gcf,[path 'uni_gradual_agrawal1_l5' str_name '.png']);
hold off

%% 10 %
%clear all
thebest = 'DP';
theworst = 'OAUE';
osnn = load('C:\Users\Rodrigo\Documents\programming\onlinessl\test_set3_neweval_uni_manifold\Agrawal_gradual_10_3_manifold.csv');
worst = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_uni_neweval\\res_%s_lab_gradual_stream3_uni_l10_vec.csv',lower(theworst)));
best = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_uni_neweval\\res_%s_lab_gradual_stream3_uni_l10_vec.csv',lower(thebest)));
htnb = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_uni_neweval\\res_%s_lab_gradual_stream3_uni_l10_vec.csv','htnb'));
osnn = smoothdata(osnn,smooth_method,smooth_window);
worst = smoothdata(worst,smooth_method,smooth_window);
best = smoothdata(best,smooth_method,smooth_window);
htnb = smoothdata(htnb,smooth_method,smooth_window);
s = length(best);
step = 4000;
figure; hold on; box on; set(gca,'fontsize',18);
title('Agrawal1-Gradual-10% of labels', 'FontSize', titfontsize);
xlabel('Time step', 'FontSize', axisfontsize);
ylabel('Prequential accuracy (%)', 'FontSize', axisfontsize);
plot((100:length(htnb)),htnb(100:end),'-.og','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((100:length(worst)),worst(100:end),':*r','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((100:length(best)),best(100:end),'--+b','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((100:length(osnn)),osnn(100:end),'-sk','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
i=step;
while i<s
    line([i i], get(gca, 'ylim'),'Color','black','LineStyle','--');
    i = i + step;
end
legend({'HTNB',theworst,thebest,'OSNN'},'Location','northwest', 'FontSize', legfontsize);
if streched
    set(gcf,'position',[1,1,1366,433]);
    set(gca,'position',[0.06588579795022,0.209459453901729,0.917276720351391,0.684189197404964]);
end
saveas(gcf,[path 'uni_gradual_agrawal1_l10' str_name '.eps'],'epsc');
saveas(gcf,[path 'uni_gradual_agrawal1_l10' str_name '.fig']);
saveas(gcf,[path 'uni_gradual_agrawal1_l10' str_name '.png']);
hold off

%% 20 %
%clear all
thebest = 'DP';
theworst = 'OAUE';
osnn = load('C:\Users\Rodrigo\Documents\programming\onlinessl\test_set3_neweval_uni_manifold\Agrawal_gradual_20_3_manifold.csv');
worst = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_uni_neweval\\res_%s_lab_gradual_stream3_uni_l20_vec.csv',lower(theworst)));
best = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_uni_neweval\\res_%s_lab_gradual_stream3_uni_l20_vec.csv',lower(thebest)));
htnb = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_uni_neweval\\res_%s_lab_gradual_stream3_uni_l20_vec.csv','htnb'));
osnn = smoothdata(osnn,smooth_method,smooth_window);
worst = smoothdata(worst,smooth_method,smooth_window);
best = smoothdata(best,smooth_method,smooth_window);
htnb = smoothdata(htnb,smooth_method,smooth_window);
s = length(best);
step = 4000;
figure; hold on; box on; set(gca,'fontsize',18);
title('Agrawal1-Gradual-20% of labels', 'FontSize', titfontsize);
xlabel('Time step', 'FontSize', axisfontsize);
ylabel('Prequential accuracy (%)', 'FontSize', axisfontsize);
plot((100:length(htnb)),htnb(100:end),'-.og','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((100:length(worst)),worst(100:end),':*r','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((100:length(best)),best(100:end),'--+b','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((100:length(osnn)),osnn(100:end),'-sk','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
i=step;
while i<s
    line([i i], get(gca, 'ylim'),'Color','black','LineStyle','--');
    i = i + step;
end
legend({'HTNB',theworst,thebest,'OSNN'},'Location','northwest', 'FontSize', legfontsize);
if streched
    set(gcf,'position',[1,1,1366,433]);
    set(gca,'position',[0.06588579795022,0.209459453901729,0.917276720351391,0.684189197404964]);
end
saveas(gcf,[path 'uni_gradual_agrawal1_l20' str_name '.eps'],'epsc');
saveas(gcf,[path 'uni_gradual_agrawal1_l20' str_name '.fig']);
saveas(gcf,[path 'uni_gradual_agrawal1_l20' str_name '.png']);
hold off

%% REAL
%% 5 %
%clear all
thebest = 'DDD';
theworst = 'RCD';
osnn = load('C:\Users\Rodrigo\Documents\programming\onlinessl\test_set3_neweval_uni_manifold\powersupplyDayNight_real_5_1_manifold.csv');
worst = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_uni_neweval\\res_%s_lab_train_powersupplyDayNight_uni_l5_vec.csv',lower(theworst)));
best = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_uni_neweval\\res_%s_lab_train_powersupplyDayNight_uni_l5_vec.csv',lower(thebest)));
htnb = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_uni_neweval\\res_%s_lab_train_powersupplyDayNight_uni_l5_vec.csv','htnb'));
osnn = smoothdata(osnn,smooth_method,smooth_window);
worst = smoothdata(worst,smooth_method,smooth_window);
best = smoothdata(best,smooth_method,smooth_window);
htnb = smoothdata(htnb,smooth_method,smooth_window);
s = length(best);
figure; hold on; box on; set(gca,'fontsize',18);
title('Power Supply - 5% of labels', 'FontSize', titfontsize);
xlabel('Time step', 'FontSize', axisfontsize);
ylabel('Prequential accuracy (%)', 'FontSize', axisfontsize);
plot((200:length(htnb)),htnb(200:end),'-.og','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((200:length(worst)),worst(200:end),':*r','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((200:length(best)),best(200:end),'--+b','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((200:length(osnn)),osnn(200:end),'-sk','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
legend({'HTNB',theworst,thebest,'OSNN'},'Location','southeast', 'FontSize', legfontsize);
if streched
    set(gcf,'position',[1,1,1366,433]);
    set(gca,'position',[0.06588579795022,0.209459453901729,0.917276720351391,0.684189197404964]);
end
saveas(gcf,[path 'uni_real_powersupplyDayNight_l5' str_name '.eps'],'epsc');
saveas(gcf,[path 'uni_real_powersupplyDayNight_l5' str_name '.fig']);
saveas(gcf,[path 'uni_real_powersupplyDayNight_l5' str_name '.png']);
hold off

%% 10%
%clear all
thebest = 'DDD';
theworst = 'RCD';
osnn = load('C:\Users\Rodrigo\Documents\programming\onlinessl\test_set3_neweval_uni_manifold\powersupplyDayNight_real_10_1_manifold.csv');
worst = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_uni_neweval\\res_%s_lab_train_powersupplyDayNight_uni_l10_vec.csv',lower(theworst)));
best = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_uni_neweval\\res_%s_lab_train_powersupplyDayNight_uni_l10_vec.csv',lower(thebest)));
htnb = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_uni_neweval\\res_%s_lab_train_powersupplyDayNight_uni_l10_vec.csv','htnb'));
osnn = smoothdata(osnn,smooth_method,smooth_window);
worst = smoothdata(worst,smooth_method,smooth_window);
best = smoothdata(best,smooth_method,smooth_window);
htnb = smoothdata(htnb,smooth_method,smooth_window);
s = length(best);
figure; hold on; box on; set(gca,'fontsize',18);
title('Power Supply - 10% of labels', 'FontSize', titfontsize);
xlabel('Time step', 'FontSize', axisfontsize);
ylabel('Prequential accuracy (%)', 'FontSize', axisfontsize);
plot((200:length(htnb)),htnb(200:end),'-.og','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((200:length(worst)),worst(200:end),':*r','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((200:length(best)),best(200:end),'--+b','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((200:length(osnn)),osnn(200:end),'-sk','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
legend({'HTNB',theworst,thebest,'OSNN'},'Location','southeast', 'FontSize', legfontsize);
if streched
    set(gcf,'position',[1,1,1366,433]);
    set(gca,'position',[0.06588579795022,0.209459453901729,0.917276720351391,0.684189197404964]);
end
saveas(gcf,[path 'uni_real_powersupplyDayNight_l10' str_name '.eps'],'epsc');
saveas(gcf,[path 'uni_real_powersupplyDayNight_l10' str_name '.fig']);
saveas(gcf,[path 'uni_real_powersupplyDayNight_l10' str_name '.png']);
hold off

%% 20 %
%clear all
thebest = 'DDD';
theworst = 'RCD';
osnn = load('C:\Users\Rodrigo\Documents\programming\onlinessl\test_set3_neweval_uni_manifold\powersupplyDayNight_real_20_1_manifold.csv');
worst = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_uni_neweval\\res_%s_lab_train_powersupplyDayNight_uni_l20_vec.csv',lower(theworst)));
best = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_uni_neweval\\res_%s_lab_train_powersupplyDayNight_uni_l20_vec.csv',lower(thebest)));
htnb = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\moavectors_uni_neweval\\res_%s_lab_train_powersupplyDayNight_uni_l20_vec.csv','htnb'));
osnn = smoothdata(osnn,smooth_method,smooth_window);
worst = smoothdata(worst,smooth_method,smooth_window);
best = smoothdata(best,smooth_method,smooth_window);
htnb = smoothdata(htnb,smooth_method,smooth_window);
s = length(best);
figure; hold on; box on; set(gca,'fontsize',18);
title('Power Supply - 20% of labels', 'FontSize', titfontsize);
xlabel('Time step', 'FontSize', axisfontsize);
ylabel('Prequential accuracy (%)', 'FontSize', axisfontsize);
plot((200:length(htnb)),htnb(200:end),'-.og','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((200:length(worst)),worst(200:end),':*r','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((200:length(best)),best(200:end),'--+b','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
plot((200:length(osnn)),osnn(200:end),'-sk','LineWidth',linewidth,'MarkerIndices',1:1000:s,'MarkerSize',markersize);
legend({'HTNB',theworst,thebest,'OSNN'},'Location','southeast', 'FontSize', legfontsize);
if streched
    set(gcf,'position',[1,1,1366,433]);
    set(gca,'position',[0.06588579795022,0.209459453901729,0.917276720351391,0.684189197404964]);
end
saveas(gcf,[path 'uni_real_powersupplyDayNight_l20' str_name '.eps'],'epsc');
saveas(gcf,[path 'uni_real_powersupplyDayNight_l20' str_name '.fig']);
saveas(gcf,[path 'uni_real_powersupplyDayNight_l20' str_name '.png']);
hold off

end