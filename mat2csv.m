labels={'5','10','20'};
art={'Sine', 'Agrawal', 'SEA', 'STAGGER'};
seq={[1,2],[3,4,5,6],[7,8],[9,10]};
for drift={'abrupt','gradual'}
    for s=1:length(art)
        for label=labels
            for idx=1:length(seq{s})
                pp=load(sprintf("C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\tes\\%s_%s_%s_%d_manifold.csv_pred_time_logs.mat",art{s},drift{1},label{1},seq{s}(idx)));
                m=[pp.pred_log', pp.truelabels_log'];
                f=sprintf("C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\ossnvectors\\pred_osnn_lab_%s_stream%d_uni_l%s.csv",drift{1},seq{s}(idx),label{1});
                dlmwrite(f,m);
            end
        end
    end
end

real = {'elec2','noaa','powersupplyDayNight','sensorClasses29and31'};
for label=labels
    for idx=1:length(real)
        pp=load(sprintf("C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\tes\\%s_real_%s_1_manifold.csv_pred_time_logs.mat",real{idx},label{1}));
        m=[pp.pred_log', pp.truelabels_log'];
        f=sprintf("C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\ossnvectors\\pred_osnn_lab_train_%s_uni_l%s.csv",real{idx},label{1});
        dlmwrite(f,m);
    end
end

visual = {'outdoor-classes-0-19','rialto-classes-0-4', 'cifar10Classes1and5', 'rmnistClasses0and1'};
for label=labels
    for idx=1:length(visual)
        pp=load(sprintf("C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\tesnonuni\\%s_visual_%s_1_manifold.csv_pred_time_logs.mat",visual{idx},label{1}));
        m=[pp.pred_log', pp.truelabels_log'];
        f=sprintf("C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\ossnvectors_visual\\pred_osnn_lab_train_%s_nonuni_l%s.csv",visual{idx},label{1});
        dlmwrite(f,m);
    end
end