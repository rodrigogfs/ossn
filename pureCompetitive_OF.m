function net = pureCompetitive_OF(net, fp, XBatch)
% OF means Opposing Forces to avoid centering of centres in a single point
c = size(net.c,1);
n = size(XBatch,1);
d = size(XBatch,2);
%% updating widths with the new batch
dist2 = squaredDistTwoSets(XBatch,net.c);
widths = (fp.sigmarate*sqrt(min(dist2))).^2; % square it back
updaterates = exp( - dist2 ./ (2*widths + 1e-5) );
%% updating centres
idxCentres = kron((1:c)',ones(n,1));
idxTrain = kron(ones(1,c),(1:n) );
updates = XBatch(idxTrain,:) - net.c(idxCentres,:);
updates = updaterates(:).*updates;
idxAcc = idxCentres(:,ones(1,d));
idxAcc = [idxAcc(:) kron((1:d)',ones(c*n,1))];
updates = accumarray(idxAcc, updates(:)) ./ (sum(updaterates)'+1e-5);
%% adding opposites forces to avoid overlapping
dist2 = squaredDistTwoSets(net.c,net.c);
dist2(1:c+1:end) = Inf;
[~,idx] = min(dist2);
Cupdates = net.c - net.c(idx,:); % this is equal to -1 * (NN - Centre)
Cupdates = rand(size(Cupdates)).*Cupdates; % randomness to spread centres as in PSO
%% Updating centres 
newupdates = updates + Cupdates;
net.c = net.c + (fp.eta*newupdates);
end