linewidth = 1;
orange = [0.8500,0.3250,0.0980]; %orange
blue = [0,0.4470,0.7410]; %blue
%% Plot of the History (log) of eta and centres for analysing sensitivity to concept drifts
%% ABRUPT - Plotting eta and centres
figure; hold on; box on;
log = load('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\history_eta_and_centres\\sensitivity_logs_l20_h50_uni_set8.mat', 'eta_log', 'centres_log');
% centres = delta_distance(log.centres_log, 300, 0);
centres = delta_distance_matrix(log.centres_log);
centres = smoothdata(centres, 'gaussian', 50);
offset = length(log.eta_log) - length(centres);
centres = [zeros(1,offset), centres];
log.eta_log = smoothdata(log.eta_log);
idx = 1:length(centres);
yyaxis right;
pcenters = plot(idx,centres(idx),'linewidth',linewidth, 'color', blue);
ylabel('$\Delta C$', 'FontSize', 22, 'interpreter', 'latex');
limit = ylim();
yyaxis left;
peta = plot(1:length(log.eta_log),log.eta_log,'linewidth',linewidth, 'color', orange);
ylim(limit);
i=4000;
while i<length(log.eta_log)
    line([i i], get(gca, 'ylim'),'Color','black','LineStyle','--','linewidth',2);
    i = i + 4000;
end
ax = gca;
ylim manual
% set(gcf,'position',[1,1,1366,441]);
% set(gca, 'position', [0.046852122986823,0.119610980912804,0.906295754026354,0.805389019087196]);
ax.FontSize = 14;
ax.YAxis(1).Color = orange;
ax.YAxis(2).Color = blue;
xlabel('Time step','FontSize', 14);
ylabel('$\eta$', 'FontSize', 22, 'interpreter', 'latex');
legend([peta,pcenters],{'$\eta$','$\Delta C$'},'Location', 'northwest', 'FontSize', 16, 'interpreter', 'latex');
saveas(gcf,'C:\Users\Rodrigo\Documents\programming\onlinessl\figs\centers_eta_sine1_20_abrupt_streched.eps','epsc');
saveas(gcf,'C:\Users\Rodrigo\Documents\programming\onlinessl\figs\centers_eta_sine1_20_abrupt_streched.fig');
saveas(gcf,'C:\Users\Rodrigo\Documents\programming\onlinessl\figs\centers_eta_sine1_20_abrupt_streched.png');
hold off

%% GRADUAL - Plotting eta and centres
figure; hold on; box on;
log = load('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\history_eta_and_centres\\sensitivity_logs_l20_h50_uni_sine1_gradual.mat', 'eta_log', 'centres_log');
% centres = delta_distance(log.centres_log, 300, 0);
centres = delta_distance_matrix(log.centres_log);
centres = smoothdata(centres, 'gaussian', 50);
offset = length(log.eta_log) - length(centres);
centres = [zeros(1,offset), centres];
log.eta_log = smoothdata(log.eta_log);
idx = 1:length(centres);
yyaxis right;
pcenters = plot(idx,centres(idx),'linewidth',linewidth, 'color', blue);
ylabel('$\Delta C$', 'FontSize', 22, 'interpreter', 'latex');
ylim([0 0.7]);
yyaxis left;
peta = plot(1:length(log.eta_log),log.eta_log,'linewidth',linewidth, 'color', orange);
ylim([0 0.7]);
i=4000;
while i<length(log.eta_log)
    line([i i], get(gca, 'ylim'),'Color','black','LineStyle','--','linewidth',2);
    i = i + 4000;
end
ax = gca;
ylim manual
% set(gcf,'position',[1,1,1366,441]);
% set(gca, 'position', [0.046852122986823,0.119610980912804,0.906295754026354,0.805389019087196]);
ax.FontSize = 14;
ax.YAxis(1).Color = orange;
ax.YAxis(2).Color = blue;
xlabel('Time step','FontSize', 14);
ylabel('$\eta$', 'FontSize', 22, 'interpreter', 'latex');
legend([peta,pcenters],{'$\eta$','$\Delta C$'},'Location', 'northwest', 'FontSize', 16, 'interpreter', 'latex');
saveas(gcf,'C:\Users\Rodrigo\Documents\programming\onlinessl\figs\centers_eta_sine1_20_gradual_streched.eps','epsc');
saveas(gcf,'C:\Users\Rodrigo\Documents\programming\onlinessl\figs\centers_eta_sine1_20_gradual_streched.fig');
saveas(gcf,'C:\Users\Rodrigo\Documents\programming\onlinessl\figs\centers_eta_sine1_20_gradual_streched.png');
hold off
return

% linewidth = 1;
% %% Plot of the History (log) of eta and centres for analysing sensitivity to concept drifts
% %% ABRUPT - Plotting eta and centres
% figure; hold on; box on;
% log = load('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\history_eta_and_centres\\sensitivity_logs_l20_h50_uni_set8.mat', 'eta_log', 'centres_log');
% % centres = delta_distance(log.centres_log, 300, 0);
% centres = delta_distance_matrix(log.centres_log);
% centres = smoothdata(centres, 'gaussian', 50);
% offset = length(log.eta_log) - length(centres);
% centres = [zeros(1,offset), centres];
% log.eta_log = smoothdata(log.eta_log);
% idx = 1:length(centres);
% plot(idx,centres(idx),'linewidth',linewidth);
% plot(1:length(log.eta_log),log.eta_log,'linewidth',linewidth);
% i=4000;
% while i<length(log.eta_log)
%     line([i i], get(gca, 'ylim'),'Color','black','LineStyle','--','linewidth',2);
%     i = i + 4000;
% end
% ax = gca;
% ylim manual
% set(gca, 'position', [0.117862371888726,0.209459453901729,0.825036603221084,0.684189197404964]);
% ax.FontSize = 16;
% xlabel('Time step','FontSize', 20);
% ylabel('Loss', 'FontSize', 20);
% % title('Codebook and learning rate variations', 'FontSize', 18);
% % legend({'$\delta_{\mathbf{c}}^{(t)} = \sum_{i}||\mathbf{c}_i^{(t)} - \mathbf{c}_i^{(t-1)}||$','$\eta^{(t)}$'},'Interpreter','latex','Location', 'northeast', 'FontSize', 14);
% legend({'Codebook','Learning rate'},'Location', 'northwest', 'FontSize', 10);
% saveas(gcf,'C:\Users\Rodrigo\Documents\programming\onlinessl\figs\centers_eta_sine1_20_abrupt_streched.eps','epsc');
% saveas(gcf,'C:\Users\Rodrigo\Documents\programming\onlinessl\figs\centers_eta_sine1_20_abrupt_streched.fig');
% saveas(gcf,'C:\Users\Rodrigo\Documents\programming\onlinessl\figs\centers_eta_sine1_20_abrupt_streched.png');
% hold off
% 
% 
% %% GRADUAL - Plotting eta and centres
% figure; hold on; box on;
% log = load('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\history_eta_and_centres\\sensitivity_logs_l20_h50_uni_sine1_gradual.mat', 'eta_log', 'centres_log');
% % centres = delta_distance(log.centres_log, 300, 0);
% centres = delta_distance_matrix(log.centres_log);
% centres = smoothdata(centres, 'gaussian', 50);
% offset = length(log.eta_log) - length(centres);
% centres = [zeros(1,offset), centres];
% log.eta_log = smoothdata(log.eta_log);
% idx = 1:length(centres);
% plot(idx,centres(idx),'linewidth',linewidth);
% plot(1:length(log.eta_log),log.eta_log,'linewidth',linewidth);
% i=4000;
% while i<length(log.eta_log)
%     line([i i], get(gca, 'ylim'),'Color','black','LineStyle','--','linewidth',2);
%     i = i + 4000;
% end
% ax = gca;
% ylim manual
% set(gca, 'position', [0.117862371888726,0.209459453901729,0.825036603221084,0.684189197404964]);
% ax.FontSize = 14;
% xlabel('Time step','FontSize', 20);
% ylabel('Loss', 'FontSize', 20);
% % title('Codebook and learning rate variations', 'FontSize', 18);
% % legend({'$\delta_{\mathbf{c}}^{(t)} = \sum_{i}||\mathbf{c}_i^{(t)} - \mathbf{c}_i^{(t-1)}||$','$\eta^{(t)}$'},'Interpreter','latex','Location', 'northeast', 'FontSize', 14);
% legend({'Codebook','Learning rate'},'Location', 'northwest', 'FontSize', 10);
% saveas(gcf,'C:\Users\Rodrigo\Documents\programming\onlinessl\figs\centers_eta_sine1_20_gradual_streched.eps','epsc');
% saveas(gcf,'C:\Users\Rodrigo\Documents\programming\onlinessl\figs\centers_eta_sine1_20_gradual_streched.fig');
% saveas(gcf,'C:\Users\Rodrigo\Documents\programming\onlinessl\figs\centers_eta_sine1_20_gradual_streched.png');
% hold off
% return

datasets = {'elec2_real_20_1', 'noaa_real_5_1', 'powerSupplyDayNight_real_10_1', 'Sine_abrupt_20_2'};
%% ELEC 2
nhidden = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\sensitivity\\nonuni_nhidden_%s_manifold.csv',datasets{1}));
batch = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\sensitivity\\nonuni_batchsize_%s_manifold.csv',datasets{1}));
nhidden = sortrows(nhidden(2:10,1:2),1);
batch = sortrows(batch(2:10,1:2),1);
figure; hold on; box on;
plot(nhidden(:,1),nhidden(:,2),'linewidth',linewidth)
plot(batch(:,1),batch(:,2),'linewidth',linewidth)
ylim([55 75]);
xlabel('Hyperparameter value','FontSize', 16);
ylabel('Prequential accuracy (%)', 'FontSize', 16);
title('Hyperparameter analysis - Elec', 'FontSize', 18);
legend({'Number of basis functions $H$','Minibatch size $N$'},'Interpreter','latex','Location', 'southwest', 'FontSize', 14);
saveas(gcf,'C:\Users\Rodrigo\Documents\programming\onlinessl\figs\sensitivity_nonuni_elec2_20.eps','epsc');
saveas(gcf,'C:\Users\Rodrigo\Documents\programming\onlinessl\figs\sensitivity_nonuni_elec2_20.fig');
saveas(gcf,'C:\Users\Rodrigo\Documents\programming\onlinessl\figs\sensitivity_nonuni_elec2_20.png');
hold off
%% NOAA
nhidden = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\sensitivity\\nonuni_nhidden_%s_manifold.csv',datasets{2}));
batch = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\sensitivity\\nonuni_batchsize_%s_manifold.csv',datasets{2}));
nhidden = sortrows(nhidden(1:11,1:2),1);
batch = sortrows(batch(1:3,1:2),1);
figure; hold on; box on;
plot(nhidden(:,1),nhidden(:,2),'linewidth',linewidth)
plot(batch(:,1),batch(:,2),'linewidth',linewidth)
ylim([55 75]);
xlabel('Hyperparameter value','FontSize', 16);
ylabel('Prequential accuracy (%)', 'FontSize', 16);
title('Hyperparameter analysis - NOAA', 'FontSize', 18);
legend({'Number of basis functions $H$','Minibatch size $N$'},'Interpreter','latex','Location', 'southeast', 'FontSize', 14);
saveas(gcf,'C:\Users\Rodrigo\Documents\programming\onlinessl\figs\sensitivity_nonuni_noaa_5.eps','epsc');
saveas(gcf,'C:\Users\Rodrigo\Documents\programming\onlinessl\figs\sensitivity_nonuni_noaa_5.fig');
saveas(gcf,'C:\Users\Rodrigo\Documents\programming\onlinessl\figs\sensitivity_nonuni_noaa_5.png');
hold off
%% Power Supply
nhidden = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\sensitivity\\nonuni_nhidden_%s_manifold.csv',datasets{3}));
batch = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\sensitivity\\nonuni_batchsize_%s_manifold.csv',datasets{3}));
nhidden = sortrows(nhidden(1:6,1:2),1);
batch = sortrows(batch(1:6,1:2),1);
figure; hold on; box on;
plot(nhidden(:,1),nhidden(:,2),'linewidth',linewidth)
plot(batch(:,1),batch(:,2),'linewidth',linewidth)
ylim([40 60]);
xlabel('Hyperparameter value','FontSize', 16);
ylabel('Prequential accuracy (%)', 'FontSize', 16);
title('Hyperparameter analysis - Power Supply', 'FontSize', 18);
legend({'Number of basis functions $H$','Minibatch size $N$'},'Interpreter','latex','Location', 'southeast', 'FontSize', 14);
saveas(gcf,'C:\Users\Rodrigo\Documents\programming\onlinessl\figs\sensitivity_nonuni_powersupply_10.eps','epsc');
saveas(gcf,'C:\Users\Rodrigo\Documents\programming\onlinessl\figs\sensitivity_nonuni_powersupply_10.fig');
saveas(gcf,'C:\Users\Rodrigo\Documents\programming\onlinessl\figs\sensitivity_nonuni_powersupply_10.png');
hold off
%% Sine 20 5
nhidden = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\sensitivity\\nonuni_nhidden_%s_manifold.csv',datasets{4}));
batch = load(sprintf('C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\sensitivity\\nonuni_batchsize_%s_manifold.csv',datasets{4}));
nhidden = sortrows(nhidden(4:22,1:2),1);
batch = sortrows(batch(2:20,1:2),1);
nhidden = nhidden(1:10,:);
batch = batch(1:10,:);
figure; hold on; box on;
plot(nhidden(:,1),nhidden(:,2),'linewidth',linewidth)
plot(batch(:,1),batch(:,2),'linewidth',linewidth)
ylim([60 80]);
xlabel('Hyperparameter value','FontSize', 16);
ylabel('Prequential accuracy (%)', 'FontSize', 16);
title('Hyperparameter analysis - Sine2', 'FontSize', 18);
legend({'Number of basis functions $H$','Minibatch size $N$'},'Interpreter','latex','Location', 'southwest', 'FontSize', 14);
saveas(gcf,'C:\Users\Rodrigo\Documents\programming\onlinessl\figs\sensitivity_nonuni_sine_20_2.eps','epsc');
saveas(gcf,'C:\Users\Rodrigo\Documents\programming\onlinessl\figs\sensitivity_nonuni_sine_20_2.fig');
saveas(gcf,'C:\Users\Rodrigo\Documents\programming\onlinessl\figs\sensitivity_nonuni_sine_20_2.png');
hold off



%% function to process the centres to be plotted
%% function to find Pricipal components from centers
function delta = delta_distance(centres_log, centre_idx, pca_enabled)
centres = [];
for i = 1:length(centres_log)
    centres = [centres; centres_log{i}(centre_idx,:)];
end
if pca_enabled
    [~,score] = pca(centres);
    centres = score(:,1:2);
end
delta = zeros(1,size(centres,1));
for i = 2:size(centres,1)
    delta(i) = sqrt(sum((centres(i,:) - centres(i-1,:)).^2));
end
end

function delta = delta_distance_matrix(centres_log)
delta = zeros(1,length(centres_log));
for i = 2:length(centres_log)
    delta(i) = sum(sqrt(sum((centres_log{i} - centres_log{i-1}).^2,2)));
end
delta = normalize(delta,'range');
end

function delta = delta_distance_max_variance(centres_log)
delta = zeros(1,length(centres_log));
for i = 2:length(centres_log)
    d = sqrt(sum((centres_log{i} - centres_log{i-1}).^2,2));
    delta(i) = max(d);
end
delta = normalize(delta,'range');
end

function delta = delta_distance_moving_average(centres_log)
delta = zeros(1,length(centres_log));
for i = 2:length(centres_log)
    d = sqrt(sum((centres_log{i} - centres_log{i-1}).^2,2));
    delta(i) = (sum(d) - mean(delta(1:i)))^2;
end
delta = normalize(delta,'range');
end