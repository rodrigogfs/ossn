function params = runSensitivity(paramfile,outfile,type,label,data,seq,method,semisup,hyperp,value)
pp=load(paramfile); % best models have already been collected
params = pp.params;
dart = '../data/nonuni/%s_matlab/stream%d.csv';
lart = '../data/nonuni/%s_matlab/lab_%s_stream%d_nonuni_l%s.csv';
% dart = '../data/%s_matlab/stream%d.csv';
% lart = '../data/%s_matlab/lab_%s_stream%d_uni_l%s.csv';
% dart = 'C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\%s\\stream%d.csv';
% lart = 'C:\\Users\\Rodrigo\\Documents\\programming\\onlinessl\\%s\\lab_%s_stream%d_uni_l%s.csv';
dreal = '../data/nonuni/real_matlab/train_%s.csv';
lreal = '../data/nonuni/real_matlab/lab_train_%s_nonuni_l%s.csv';
% dreal = '../data/realstreams/train_%s.csv';
% lreal = '../data/realstreams/lab_train_%s_uni_l%s.csv';
dextra = '../data/extra/extra_matlab/%s.csv';
lextra = '../data/extra/extra_matlab/lab_train_%s_l%s.csv';
% executing final run
if strcmpi(type,'real')
    searchdata = sprintf(dreal,data);
    searchlab = sprintf(lreal,data,label);
elseif strcmpi(type,'gradual') || strcmpi(type,'abrupt') 
    searchdata = sprintf(dart,type,seq);
    searchlab = sprintf(lart,type,type,seq,label);
elseif strcmpi(type,'extra') 
    searchdata = sprintf(dextra,data);
    searchlab = sprintf(lextra,data,label);
else
    error('Invalid stream type');
end
% all sequences of files with dataset
%params.(t{1}).(d{1}).(m{1}).nhidden = params.(t{1}).(d{1}).(m{1}).nhidden + 200;
if strcmpi(type,'real')
    pt='real';
elseif strcmpi(type,'gradual') || strcmpi(type,'abrupt') 
    pt='art';
elseif strcmpi(type,'extra')
    pt='extra';
else
    error('Invalid stream type');
end
params.(pt).(data).(method).nhidden = 50; % These fixed values are for speed
params.(pt).(data).(method).batchsize = 50;
params.(pt).(data).(method).(hyperp) = value; % This is the evaluated hyperparameter
preqvector=runOnlineSSL(searchdata,searchlab,semisup,params.(pt).(data).(method));
preqmean = mean(preqvector*100);
preqstd = std(preqvector*100);
fid = fopen(outfile,'a');
fprintf(fid,'%.2f,%.2f,%.2f\n',value,preqmean,preqstd);
fclose(fid);
end