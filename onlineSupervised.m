function [preqacc, net] = onlineSupervised(XTrain,YTrain,labelmask,fp,drawplot)
%ONLINESSL This is the training for online SSL for a single batch
%   This includes unsupervised and semissupervised training
%   XTtrain is the complete data stream
%   YTrain is the true labels
%   labels are binary masks

%% creating targets from class labels
disp(datetime); % keeping track of time
n = size(XTrain,1);
d = size(XTrain,2);
t = 1;
net = [];
preqacc = zeros(n-1,1);
preqS = 0;
preqN = 0;
fadingfactor = 0.999;
fp.lambda = 0; % supervised

while t<n
    window = max([1 t+1-fp.batchsize]):t;
    XBatch = XTrain(window,:);
    YBatch = YTrain(window);
    labelsBatch = labelmask(window);
    lBatch = XBatch(labelsBatch,:);
    uBatch = zeros(0,d); % This makes it treat unlabelled data as empty matrix
    yBatch = YBatch(labelsBatch);
    
    if ~isempty(lBatch) % trains only if there are labels
        %% Centre learning
        if t > 1 && net.nhidden >= fp.nhidden
            if strcmpi(fp.method,'purecomp')
                net = pureCompetitive(net, fp, XBatch);  
                net = MLVQ_Flex(net, lBatch, yBatch, uBatch);
            elseif strcmpi(fp.method,'purecomp_OF')
                net = pureCompetitive_OF(net, fp, XBatch);
                net = MLVQ_Flex(net, lBatch, yBatch, uBatch);
            elseif strcmpi(fp.method,'SLVQ')
                net = SLVQ(net, fp, lBatch, yBatch, uBatch);
                net = MLVQ_Flex(net, lBatch, yBatch, uBatch);
            elseif strcmpi(fp.method,'SLVQ_OF')
                net = SLVQ_OF(net, fp, lBatch, yBatch, uBatch);
                net = MLVQ_Flex(net, lBatch, yBatch, uBatch);
            elseif strcmpi(fp.method,'manifold')
                net = MLVQ_Flex(net, lBatch, yBatch, uBatch);
            elseif strcmpi(fp.method,'id') ||  strcmpi(fp.method,'identity')
                net = identityUpdate(net, XBatch);
            else
                error('Invalid centre update method');
            end
        else
            net = generateRBFN(net,d,XBatch);
        end
        %% Width learning
        net = updateWidths(net, fp);
        %% SemiSupervised training
        if strcmpi(fp.methodw,'NR')
            net = trainOnlineSSL_NR(net, fp, lBatch, yBatch, uBatch);
        elseif strcmpi(fp.methodw,'NAG')
            net = trainOnlineSSL_NAG(net, fp, lBatch, yBatch, uBatch);
        elseif strcmpi(fp.methodw,'SGD')
            net = trainOnlineSSL_SGD(net, fp, lBatch, yBatch, uBatch);
        else
            error('Invalid weight update method');
        end
    end
    
    %% Checking if there is net
    if isempty(net)
        net = generateRBFN(net,d,XBatch);
        net = updateWidths(net, fp);
    end
    %% Computing prequential error on next instance
    XTestBatch = XTrain(t+1,:);
    YTestBatch = YTrain(t+1);
    testf = rbfforward(net,XTestBatch);
    Idx = testf>0.5;
    testacc = (Idx==YTestBatch);
    preqS = testacc + fadingfactor*preqS;
    preqN = 1 + fadingfactor*preqN;
    %preqacc vector has one position less since the rbf is not defined for the
    %very first instance of the stream
    preqacc(t) = preqS/preqN;
    
    %% incremeting time steps
    t = t + 1;
    
    if mod(t,1000)==0
        fprintf('Time step %d - Mean Supervised Accuracy %.2f \n',t, 100*mean(preqacc));
        disp(datetime); % keeping track of time
        if drawplot==1
            plot(preqacc(1:t)); drawnow;
        end
    end
    %% Plotting the output
    if drawplot>1 && mod(t,100)==0
        figure(1); hold on;
        testmin = min(XTrain);
        testmax = max(XTrain);
        axis([testmin(1) testmax(1) testmin(2) testmax(2)]);
%         axis tight
        % plotting entire stream
        % unlabelled instances
        plot(XTrain(:,1),XTrain(:,2),'+k');
        % plotting graph
        dist2 = squaredDistTwoSets([net.c; uBatch],[net.c; uBatch]);
        dist2(dist2==0)=Inf;
        sigma = fp.sigmamanif*sqrt(min(dist2)) + 1e-5;
        S = exp(- dist2 ./ (2*sigma.^2) );
%             m=maxk(S,5,2);
%             S(S<min(m,[],2))=0;
        [P1, P2] = gplot(S, [net.c; uBatch]);
        plot(P1, P2, '-sg', ...
            'LineWidth',.5,...
            'MarkerEdgeColor','k',...
            'MarkerFaceColor',[.49 1 .63],...
            'MarkerSize',10);
        plot(XTrain(labelmask & YTrain==0,1),XTrain(labelmask & YTrain==0,2),'ob','LineWidth',2.5); % class 0
        plot(XTrain(labelmask & YTrain==1,1),XTrain(labelmask & YTrain==1,2),'or','LineWidth',2.5); % class 1
        % test instance
        if YTrain(t) == 0
            % class 0 = blue diamond
            plot(XTrain(t,1),XTrain(t,2),'db','LineWidth',5,'MarkerEdgeColor','k');
        else
            % class 1 = black diamond
            plot(XTrain(t,1),XTrain(t,2),'dr','LineWidth',5,'MarkerEdgeColor','k');
        end
        title(['Predictions It:' num2str(t)]);
        h = gca;
        a = get(h,'Xlim');
        b = get(h,'Ylim');
        box = [a b];
        % Visualise the results
        gsteps        = 50;
        range1        = box(1):(box(2)-box(1))/(gsteps-1):box(2);
        range2        = box(3):(box(4)-box(3))/(gsteps-1):box(4);
        [grid1, grid2]    = meshgrid(range1,range2);
        Xgrid        = [grid1(:) grid2(:)];
        output = rbfforward(net,Xgrid);
        contour(range1,range2,reshape(output,size(grid1)),[0.5 0.5],'-','Color','r','LineWidth',2.5);
        % plot center radius
        if drawplot>2
            for i=1:net.nhidden
                dist2 = squaredDistTwoSets(Xgrid, net.c(i,:));
                Phi = exp(- (dist2 ./ (2*net.width(i)) ));
                contour(range1,range2,reshape(Phi,size(grid1)),[0.5 0.5]);
            end
        end
        
        hold off;
        drawnow; if t<n, clf; end
    end
end
end