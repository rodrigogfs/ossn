function params = findBestParams(searchdir)
%% obtain best params - model selection
types = {'art', 'real', 'extra', 'visual'};
% artdatasets = {'Sine', 'Agrawal', 'SEA', 'STAGGER'};
% realdatasets = {'elec2','noaa','powersupplyDayNight','sensorClasses29and31','weatherAUSprep'};
artdatasets = {'Sine', 'Agrawal', 'SEA', 'STAGGER'};
realdatasets = {'elec2','noaa','powersupplyDayNight','sensorClasses29and31', 'weatherAUSprep'};
visualdatasets = {'cifar10Classes1and5','rmnistClasses0and1','outdoor-classes-0-19','rialto-classes-0-4'};
extradatasets = {'g241d', 'digit1'};
maptypesdata = struct();
maptypesdata.('art')=artdatasets;
maptypesdata.('real')=realdatasets;
maptypesdata.('extra')=extradatasets;
maptypesdata.('visual')=visualdatasets;
%methods = {'manifold', 'purecomp', 'slvq', 'purecomp_OF', 'SLVQ_OF', 'id'};
methods = {'manifold'};
searchstr = [searchdir, '%s_%s_%s_*'];
params = struct();
for t=types
    for d=maptypesdata.(t{1})
        for m=methods
            search = sprintf(searchstr,t{1},d{1},m{1});
            listfiles = dir(search);
            bestacc=0;bestfp=[];
            for f=1:length(listfiles)
                p=load([listfiles(f).folder, filesep, listfiles(f).name]);
                if p.preqacc > bestacc
                    bestacc = p.preqacc;
                    bestfp = p.fp;
                end
            end
            trim_data_name = replace(d{1},'-','');
            params.(t{1}).(trim_data_name).(m{1}) = bestfp;
            disp(['Best for ' t{1} ' ' d{1} ' ' m{1}]);
            disp(bestacc);
            disp(params.(t{1}).(trim_data_name).(m{1}));
        end
    end
end
end