function bestFP = randomizedSearch(method,budget,Xfile,labelsFile,resultFile)
%randomizedSearch Model Selection by Randomized Search
rng('shuffle'); % random seed generator
X = dlmread(Xfile,',',1,0);
labelmask = logical(load(labelsFile));
Y = X(:,end);
X = X(:,1:end-1);
n = size(X,1);
if exist(resultFile,'file')==2 % checking if we are resuming a previous search
    load(resultFile,'bestFP','bestPreq');
else
    bestFP = [];
    bestPreq = -Inf;
end
for i=1:budget
    fprintf('*****************************************\n');
    fprintf('iteration %d\n',i);
    disp('best accuracy and hyperparameters so far'); disp(bestPreq); disp(bestFP);
    fp = struct;
    fp.sigmarate = rand*10 + 1e-5;
    fp.sigmamanif = rand*10 + 1e-5;
    fp.eta = rand + 1e-5;
    fp.alpha = rand + 1e-5;
    fp.epochs = 1000;
    fp.lambda = rand + 1e-5;
    fp.width = rand + 1e-5;
    fp.nhidden = randi( [10 min([n 100])] );
    fp.batchsize = randi( [10 min([n 100])] );
    fp.method = method;
    disp('current FP'); disp(fp);
    try
        preqvector = onlineSSL(X,Y,labelmask,fp,0);
        preqacc = mean(preqvector); % not sampling at every 500 steps
        if preqacc>bestPreq
            bestPreq = preqacc;
            bestFP = fp;
            save(resultFile,'bestFP','bestPreq','preqvector');
        end
    catch ME
        disp('An error ocurred with this configuration');
        disp(ME.message);
    end        
end
% [filepath,name,ext] = fileparts(Xfile);
% save([filepath filesep 'best_' method '_' name ext],'fp');
% save(resultFile,'bestFP','bestPreq');
