function [preqvector, preqacc, preqstd, eta_log, centres_log, pred_log, truelabels_log, time_log] = runOnlineSSL(Xfile,labelsFile,semisup,fp,resultFile,method,methodw,...
    sigmarate,sigmamanif,eta,etaw,beta,alpha,epochs,lambda,width,nhidden,batchsize,seed)
%RUNONLINESSL Runs onlineSSL once without using a struct as parameter container.
%Ideal for paralelizing randomized search.
if nargin > 4
    % Setting the random seed
    rng(seed);
    % building parameters struct
    fp=struct;
    fp.sigmarate=sigmarate;
    fp.sigmamanif=sigmamanif;
    fp.eta=eta;
    fp.alpha=alpha;
    fp.epochs=epochs;
    fp.lambda=lambda;
    fp.width=width;
    fp.nhidden=nhidden;
    fp.batchsize=batchsize;
    fp.method=method;
    fp.methodw=methodw;
    fp.etaw=etaw;
    fp.beta=beta;
    fp.seed=seed;
else
    rng(fp.seed);
end
%% reading files and preparing data
X = load(Xfile);
labelmask = logical(load(labelsFile));
Y = X(:,end);
X = X(:,1:end-1);
%% running onlinessl
tic;
if semisup
    [preqvector, ~, eta_log, centres_log, pred_log, truelabels_log] = onlineSSL(X,Y,labelmask,fp,0);
else
    preqvector = onlineSupervised(X,Y,labelmask,fp,0);
end
time_log = toc;
preqacc = mean(preqvector); % sampling at every steps of labelled points
preqstd = std(preqvector);
if nargin > 4 && ~isempty(resultFile)
    save(resultFile,'fp','preqacc','preqstd');
end
end