function net = updateWidths(net,fp)
% Online Update for RBF widths
d = sqrt(squaredDistTwoSets(net.c,net.c));
net.width = (fp.width*mean(d)).^2 + 1e-5; % These must be SQUARED widths
end