function [preqacc, net] = OSNN(XTrain,YTrain,labelmask,fp,drawplot)
%ONLINESSL This is the training for online SSL for a single batch
%   This includes unsupervised and semissupervised training
%   XTtrain is the complete data stream
%   YTrain is the true labels
%   labels are binary masks

%% creating targets from class labels
disp(datetime); % keeping track of time
n = size(XTrain,1);
d = size(XTrain,2);
t = 1;
net = [];
preqacc = zeros(n-1,1);
preqS = 0;
preqN = 0;
fadingfactor = 0.999;
while t<n
    window = max([1 t+1-fp.batchsize]):t;
    XBatch = XTrain(window,:);
    YBatch = YTrain(window);
    labelsBatch = labelmask(window);
    lBatch = XBatch(labelsBatch,:);
    uBatch = XBatch(~labelsBatch,:);
    yBatch = YBatch(labelsBatch);
    %% Centre learning
    if t > 1 && net.nhidden >= fp.nhidden
        if strcmpi(fp.method,'purecomp')
            net = pureCompetitive(net, fp, XBatch);  
            net = SLVQ(net, lBatch, yBatch, uBatch);
        elseif strcmpi(fp.method,'purecomp_OF')
            net = pureCompetitive_OF(net, fp, XBatch);
            net = SLVQ(net, lBatch, yBatch, uBatch);
        elseif strcmpi(fp.method,'NG_SLVQ')
            net = NG_SLVQ(net, fp, lBatch, yBatch, uBatch);
            net = SLVQ(net, lBatch, yBatch, uBatch);
        elseif strcmpi(fp.method,'NG_SLVQ_OF')
            net = NG_SLVQ_OF(net, fp, lBatch, yBatch, uBatch);
            net = SLVQ(net, lBatch, yBatch, uBatch);
        elseif strcmpi(fp.method,'manifold')
            net = SLVQ(net, lBatch, yBatch, uBatch);
        elseif strcmpi(fp.method,'id') ||  strcmpi(fp.method,'identity')
            net = identityUpdate(net, XBatch);
        else
            error('Invalid centre update method');
        end
    else
        net = generateRBFN(net,d,XBatch);
    end
    %% Width learning
    net = updateWidths(net, fp);
    %% SemiSupervised training
    if strcmpi(fp.methodw,'NR')
        net = trainOnlineSSL_NR(net, fp, lBatch, yBatch, uBatch);
    elseif strcmpi(fp.methodw,'NAG')
        net = trainOnlineSSL_NAG(net, fp, lBatch, yBatch, uBatch);
    elseif strcmpi(fp.methodw,'SGD')
        net = trainOnlineSSL_SGD(net, fp, lBatch, yBatch, uBatch);
    else
        error('Invalid weight update method');
    end
    
    %% Computing prequential error on next instance
    XTestBatch = XTrain(t+1,:);
    YTestBatch = YTrain(t+1);
    testf = rbfforward(net,XTestBatch);
    Idx = testf>0.5;
    testacc = (Idx==YTestBatch);
    preqS = testacc + fadingfactor*preqS;
    preqN = 1 + fadingfactor*preqN;
    %preqacc vector has one position less since the rbf is not defined for the
    %very first instance of the stream
    preqacc(t) = preqS/preqN;
    
    %% incremeting time steps
    t = t + 1;
    
    if mod(t,1000)==0
        fprintf('Time step %d - Mean SemiSup Accuracy %.2f \n',t, 100*mean(preqacc(1:t-1)));
        disp(datetime); % keeping track of time
        if drawplot==1
            plot(preqacc(1:t-1)); drawnow;
        end
    end
    
end
end