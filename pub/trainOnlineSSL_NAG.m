function [net, loss] = trainOnlineSSL_NAG(net, fp, Xlab, lY, Xunl)
% This is the supervised part of the training algorithm (SGD with Nesterov's momentum)
% and l2 regularization
Xunl = [net.c; Xunl]; % centres are considered unlabeled instances
lPhi=designmatrix(net,Xlab);
uPhi=designmatrix(net,Xunl);
numL = size(Xlab,1); numU=size(Xunl,1);
if numL
    m = 1/numL + 1/numU;
else
    m = 1/numU;
end
%% Calculating similarities for pseudolabels
% The labelled instances are considered for the neighborhood
dist2 = squaredDistTwoSets(Xunl,[Xlab; Xunl]);
dist2(dist2==0)=Inf;
sigma = fp.sigmamanif*sqrt(min(dist2,[],2)) + 1e-8;
S = exp(- dist2 ./ (2*sigma.^2) );

%% f after adding velocity
if ~isfield(net,'vel') || size(net.vel,1)~=size(net.w,1) % use momentum iff centres are complete
    net.vel=0;
    net.vel_b=0;
end
step = net.w + fp.beta*net.vel; % step for gradient calculation
step_b = net.bias + fp.beta*net.vel_b; % step for gradient calculation
lF=sigmoid(lPhi * step + step_b);
uF=sigmoid(uPhi * step + step_b);

%% pseudo-labels
u = (S*[lY; uF]) ./ (sum(S,2)+1e-5); % with safeguard

%% gradient one step ahead
uG = fp.lambda * (uPhi' * ( ( uF - u)) ) / numU;
uG_b = fp.lambda * sum(uF - u) / numU;
if numL
    lG = (lPhi' * (lF-lY)) / numL;
    lG_b = sum(lF-lY) / numL;
    G = lG + uG + fp.alpha*m*net.w;
    G_b = lG_b + uG_b; % no L2 for bias
else
    G = uG + fp.alpha*m*net.w;
    G_b = uG_b;
end
%% optimising best fit of RBF using eta (Newton-Raphson line search)
velocity = fp.beta*net.vel - fp.etaw*G;
velocity_b = fp.beta*net.vel_b - fp.etaw*G_b;
net.w = net.w + velocity;
net.bias = net.bias + velocity_b;
lF=sigmoid(lPhi*net.w + net.bias);
uF=sigmoid(uPhi*net.w + net.bias);
% current loss
lossU = - fp.lambda*sum(sum(u.*log(uF))) / numU;
if numL
    lossL = - sum(sum(lY.*log(lF))) / numL;
    loss = lossL + lossU + 0.5*m*fp.alpha*sum(net.w.^2);
else
    loss = lossU + 0.5*m*fp.alpha*sum(net.w.^2);
end
net.vel = velocity;
net.vel_b = velocity_b;
end

function Phi = designmatrix(net, X)
% Calculate squared norm matrix, of dimension (ndata, ncentres)
dist2 = squaredDistTwoSets(X, net.c);
% Calculate width factors: net.wi contains squared widths
% Now compute the activations
Phi = exp(- (dist2 ./ (2*net.width) ));
end