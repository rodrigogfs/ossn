function net = NG_SLVQ(net, fp, lBatch, lY, uBatch)
% This is competitive hebbian learning. It considers that centres have classes 
% How to assign classes to centres?
% A mix of LVQ3 and Neural Gas in order to deal with semisupervised
% learning
c = size(net.c,1);
[n, d] = size(uBatch);
if ~isempty(uBatch)
    %% updating widths with the new batch
    dist2 = squaredDistTwoSets(uBatch,net.c);
    widths = (fp.sigmarate*sqrt(min(dist2))).^2;
    updaterates = exp( - dist2 ./ (2*widths+1e-5) );
    %% updating centres according to unlabelled data
    idxCentres = kron((1:c)',ones(n,1));
    idxTrain = kron(ones(1,c),(1:n) );
    Uupdates = uBatch(idxTrain,:) - net.c(idxCentres,:);
    Uupdates = updaterates(:).*Uupdates;
    idxAcc = idxCentres(:,ones(1,d));
    idxAcc = [idxAcc(:) kron((1:d)',ones(c*n,1))];
    Uupdates = accumarray(idxAcc, Uupdates(:)) ./ (sum(updaterates)+1e-5)';
else
    Uupdates=0;
end
if ~isempty(lBatch)
    %% updating centres according to labelled data
    % assigning labels to centres using the network output
    centreClasses = (rbfforward(net,net.c)>0.5);
    n = size(lBatch,1);
    dist2 = squaredDistTwoSets(lBatch,net.c);
    widths = (fp.sigmarate*sqrt(min(dist2))).^2;
    updaterates = exp( - dist2 ./ (2*widths+1e-5) );
    idxCentres = kron((1:c)',ones(n,1));
    idxTrain = kron(ones(1,c),(1:n) )';
    Lupdates = lBatch(idxTrain,:) - net.c(idxCentres,:);
    % LVQ depends on the correct class assignments
    updatesign = (lY(idxTrain) == centreClasses(idxCentres))*2 - 1;
    Lupdates = (updatesign.*updaterates(:)).*Lupdates;
    idxAcc = idxCentres(:,ones(1,d));
    idxAcc = [idxAcc(:) kron((1:d)',ones(c*n,1))];
    Lupdates = accumarray(idxAcc, Lupdates(:)) ./ (sum(updaterates)+1e-5)';
else
    Lupdates=0;
end
%% Updating centres
updates = Lupdates + Uupdates;
% updates = updates ./ (sqrt(sum(updates.^2,2))+1e-5); % normalizing
net.c = net.c + (fp.eta*updates);
end