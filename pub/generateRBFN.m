function net = generateRBFN(net,D,XTrain)
% generates a RBFN for binary classification with A SINGLE output. We
% assume the number of centres is equal to the size of the batch
if isempty(net)
    net = struct;
    net.nin = D;
    net.nout = 1; % Binary classification with one output neuron
    net.c = XTrain(end,:) + 0.05*randn(1,D);
    net.w = 0.05*randn(1,1); % a single weight connection
    net.bias = randn(1,net.nout);
else
    net.c = [net.c; XTrain(end,:)+0.05*randn(1,D)];
    net.w = [net.w; 0.05*randn(1,1)];
end
net.nhidden = size(net.c,1);
% The widths will be calculated in the next step