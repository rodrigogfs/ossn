function [preqvector, preqacc, preqstd] = runOSNN(Xfile,labelsFile,fp,resultFile,method,methodw,...
    sigmarate,sigmamanif,eta,etaw,beta,alpha,epochs,lambda,width,nhidden,batchsize,seed)
%RUNONLINESSL Runs onlineSSL once without using a struct as parameter container.
% Example of use:
% load('FP_Sine_manif.mat');
% [preqvector, preqacc, preqstd] = runOSNN('stream1.csv','lab_abrupt_stream1_nonuni_l5.csv',fp);
if nargin > 4
    % Setting the random seed
    rng(seed);
    % building parameters struct
    fp=struct;
    fp.sigmarate=sigmarate;
    fp.sigmamanif=sigmamanif;
    fp.eta=eta;
    fp.alpha=alpha;
    fp.epochs=epochs;
    fp.lambda=lambda;
    fp.width=width;
    fp.nhidden=nhidden;
    fp.batchsize=batchsize;
    fp.method=method;
    fp.methodw=methodw;
    fp.etaw=etaw;
    fp.beta=beta;
    fp.seed=seed;
else
    rng(fp.seed);
end
%% reading files and preparing data
X = load(Xfile);
labelmask = logical(load(labelsFile));
Y = X(:,end);
X = X(:,1:end-1);
%% running onlinessl
preqvector = OSNN(X,Y,labelmask,fp,1);
preqacc = mean(preqvector); % sampling at every steps of labelled points
preqstd = std(preqvector);
if nargin > 4 && ~isempty(resultFile)
    save(resultFile,'fp','preqacc','preqstd');
end
end