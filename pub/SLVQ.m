function net = SLVQ(net, lBatch, yBatch, uBatch)
% This version adjusts the center when the batch has only labelled,
% only unlabelled or both types of instances. The other version only adapts when
% both are present
l = size(lBatch,1);
u = size(uBatch,1);
if l
    dist2L = squaredDistTwoSets(lBatch,net.c);
    [~,nnL] = min(dist2L,[],2);
end
if u
    dist2U = squaredDistTwoSets(uBatch,net.c);
    [~,nnU] = min(dist2U,[],2);
end
for i=1:net.nhidden
    centre = 0;
    scaling = 0;
    if l
        regionLidx0 = ((nnL == i) & (yBatch == 0));
        regionLidx1 = ((nnL == i) & (yBatch == 1));
        if sum(regionLidx0) > sum(regionLidx1)
            regionMaj = lBatch(regionLidx0,:);
            regionMin = lBatch(regionLidx1,:);
        else
            regionMaj = lBatch(regionLidx1,:);
            regionMin = lBatch(regionLidx0,:);
        end
        sumMaj = sum(regionMaj,1) / l;
        sumMin = sum(regionMin,1) / l;
        scaling = ((size(regionMaj,1) - size(regionMin,1)) / l);
        centre = sumMaj - sumMin;
    end
    if u
        regionUidx = (nnU == i);
        regionU = uBatch(regionUidx,:);
        sumU = sum(regionU,1) / u;
        scaling = scaling + (size(regionU,1) / u);
        centre = centre + sumU;
    end
    if scaling>0
        net.c(i,:) = centre / scaling;
    end
end