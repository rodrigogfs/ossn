function net = pureCompetitive(net, fp, XBatch)
c = size(net.c,1);
n = size(XBatch,1);
d = size(XBatch,2);
%% updating widths with the new batch
dist2 = squaredDistTwoSets(XBatch,net.c);
widths = (fp.sigmarate*sqrt(min(dist2))).^2; % square it back
updaterates = exp( - dist2 ./ (2*widths + 1e-5) );
%% updating centres
idxCentres = kron((1:c)',ones(n,1));
idxTrain = kron(ones(1,c),(1:n) );
updates = XBatch(idxTrain,:) - net.c(idxCentres,:);
updates = updaterates(:).*updates;
idxAcc = idxCentres(:,ones(1,d));
idxAcc = [idxAcc(:) kron((1:d)',ones(c*n,1))];
updates = accumarray(idxAcc, updates(:)) ./ (sum(updaterates)'+1e-5);
%% Updating centres 
net.c = net.c + (fp.eta*updates);
end