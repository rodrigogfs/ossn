function [net, loss] = trainOnlineSSL_SGD(net, fp, Xlab, lY, Xunl)
% This is the supervised part of the training algorithm (standard SGD)
% and l2 regularization
loss = zeros(fp.epochs,1);
Xunl = [net.c; Xunl]; % centres are considered unlabeled instances
[lF, lPhi]=rbfforward(net,Xlab);
[uF, uPhi]=rbfforward(net,Xunl);

%% Calculating similarities for pseudolabels
% The labelled instances are considered for the neighborhood
dist2 = squaredDistTwoSets(Xunl,[Xlab; Xunl]);
dist2(dist2==0)=Inf;
sigma = fp.sigmamanif*sqrt(min(dist2,[],2)) + 1e-5;
S = exp(- dist2 ./ (2*sigma.^2) );

%% pseudo-labels
u = (S*[lY; uF]) ./ (sum(S,2)+1e-5); % with safeguard
numL = size(Xlab,1); numU=size(Xunl,1);
if numL
    m = 1/numL + 1/numU;
else
    m = 1/numU;
end

%% Main loop
for i=1:fp.epochs
    %% gradient
    uG = (fp.lambda/numU) * (uPhi' * ( ( uF - u)) );
    uG_b = fp.lambda * sum(uF - u) / numU; 
    if numL
        lG = (lPhi' * (lF-lY)) / numL;
        lG_b = sum(lF-lY) / numL;
        G = lG + uG;
        G_b = lG_b + uG_b;
    else
        G = uG;  % if there is no labelled instance in this batch
        G_b = uG_b;
    end
    %% updating weights
    net.w = (1 - fp.etaw*fp.alpha*m)*net.w - fp.etaw*G;
    net.bias = net.bias - fp.etaw*G_b;
    lF=sigmoid(lPhi*net.w + net.bias);
    uF=sigmoid(uPhi*net.w + net.bias);
    % current loss
    lossU = - fp.lambda*sum(sum(u.*log(uF))) / numU;
    if numL
        lossL = - sum(sum(lY.*log(lF))) / numL;
        loss(i) = lossL + lossU + (0.5*fp.alpha*m*sum(net.w.^2));
    else % if there is no labelled instance in this batch
        loss(i) = lossU + (0.5*fp.alpha*m*sum(net.w.^2));
    end
    % updating eta
    % net.etaw = 0.95 * net.etaw;
end
end