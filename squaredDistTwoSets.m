function D = squaredDistTwoSets(X,Y)
    % old code
    % dist = sqrt( sum(X.^2,2)*ones(1,numL) + ones(numU,1)*sum( Y.^2, 2 )' - 2.*X*Y' );
    D = sum(X.^2,2) + sum( Y.^2, 2 )' - 2.*X*Y';
    % Rounding errors occasionally cause negative entries in D
    D(D<0) = 0;
end
